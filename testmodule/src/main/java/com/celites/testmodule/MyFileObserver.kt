package com.celites.testmodule

import android.os.FileObserver
import android.util.Log

/**
 * Created by Prasham on 1/7/2016.
 */
class MyFileObserver(var absolutePath: String) : FileObserver(absolutePath, FileObserver.ALL_EVENTS) {

    @Override
    fun onEvent(event: Int, path: String?) {
        if (path == null) {
            return
        }
        //a new file or subdirectory was created under the monitored directory
        if (FileObserver.CREATE and event !== 0) {
            val s = "$absolutePath/$path is created\n"
            FileAccessLogStatic.accessLogMsg += s
            Log.d(TAG, s)
        }
        //a file or directory was opened
        if (FileObserver.OPEN and event !== 0) {
            val s = path + " is opened\n"
            FileAccessLogStatic.accessLogMsg += s
            Log.d(TAG, s)
        }
        //data was read from a file
        if (FileObserver.ACCESS and event !== 0) {
            val s = "$absolutePath/$path is accessed/read\n"
            FileAccessLogStatic.accessLogMsg += s
            Log.d(TAG, s)
        }
        //data was written to a file
        if (FileObserver.MODIFY and event !== 0) {
            val s = "$absolutePath/$path is modified\n"
            FileAccessLogStatic.accessLogMsg += s
            Log.d(TAG, s)
        }
        //someone has a file or directory open read-only, and closed it
        if (FileObserver.CLOSE_NOWRITE and event !== 0) {
            val s = path + " is closed\n"
            FileAccessLogStatic.accessLogMsg += s
            Log.d(TAG, s)
        }
        //someone has a file or directory open for writing, and closed it
        if (FileObserver.CLOSE_WRITE and event !== 0) {
            val s = "$absolutePath/$path is written and closed\n"
            FileAccessLogStatic.accessLogMsg += s
            Log.d(TAG, s)
        }
        //[todo: consider combine this one with one below]
        //a file was deleted from the monitored directory
        if (FileObserver.DELETE and event !== 0) {
            //for testing copy file
            //			FileUtils.copyFile(absolutePath + "/" + path);
            val s = "$absolutePath/$path is deleted\n"
            FileAccessLogStatic.accessLogMsg += s
            Log.d(TAG, s)
        }
        //the monitored file or directory was deleted, monitoring effectively stops
        if (FileObserver.DELETE_SELF and event !== 0) {
            val s = absolutePath + "/" + " is deleted\n"
            FileAccessLogStatic.accessLogMsg += s
            Log.d(TAG, s)
        }
        //a file or subdirectory was moved from the monitored directory
        if (FileObserver.MOVED_FROM and event !== 0) {
            val s = "$absolutePath/$path is moved to somewhere \n"
            FileAccessLogStatic.accessLogMsg += s
            Log.d(TAG, s)
        }
        //a file or subdirectory was moved to the monitored directory
        if (FileObserver.MOVED_TO and event !== 0) {
            val s = "File is moved to $absolutePath/$path\n"
            FileAccessLogStatic.accessLogMsg += s
            Log.d(TAG, s)
        }
        //the monitored file or directory was moved; monitoring continues
        if (FileObserver.MOVE_SELF and event !== 0) {
            val s = path + " is moved\n"
            FileAccessLogStatic.accessLogMsg += s
            Log.d(TAG, s)
        }
        //Metadata (permissions, owner, timestamp) was changed explicitly
        if (FileObserver.ATTRIB and event !== 0) {
            val s = "$absolutePath/$path is changed (permissions, owner, timestamp)\n"
            FileAccessLogStatic.accessLogMsg += s
            Log.d(TAG, s)
        }
    }

    companion object {

        private val TAG = "MyFileObserver"
    }
}
