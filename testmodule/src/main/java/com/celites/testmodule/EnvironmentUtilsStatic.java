package com.celites.testmodule;

import android.os.Environment;

/**
 * Created by Prasham on 1/7/2016.
 */
public class EnvironmentUtilsStatic {
	public static boolean is_external_storage_available() {
		return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
	}
}
