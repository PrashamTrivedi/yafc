package com.celites.yafc.kotlin.files.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.celites.kutils.d
import com.celites.kutils.isEmptyString
import com.celites.kutils.v
import com.celites.yafc.R
import com.celites.yafc.kotlin.Utils.extractBundleData
import com.celites.yafc.kotlin.data.DataRepository
import com.celites.yafc.kotlin.files.deleteFromPath
import com.google.firebase.analytics.FirebaseAnalytics

/**
 * Created by Prasham on 9/9/2016.
 */
class WatchReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val action = intent?.action ?: ""
        d("WatchReceiver", "Action $action")
        if (!action.isEmptyString()) {
            context?.let {
                val bundle = intent?.extras

                bundle?.let {
                    v("WatchReceiver ${bundle.extractBundleData()}")

                    val path = bundle.getString(context.getString(R.string.bundle_selected_path)) ?: ""
                    val selectedFiles = bundle.getStringArray(context.getString(R.string.bundle_selected_files)) ?: emptyArray()
                    for (string in selectedFiles) {
                        d("Checking string $string")
                    }
                    val dataRepo = DataRepository().initWith(context)
                    val tabModel = dataRepo.getTabByPath(path)
                    v("NukeReceiver", "Awoken to $action for $path")
                    if (action.equals(context.getString(R.string.action_delete), true)) {
                        if (!path.isEmptyString()) {
                            tabModel?.let {

                                path.deleteFromPath(tabModel.name, tabModel.id, context, selectedFiles)
                                val analyticsBundle = Bundle()
                                analyticsBundle.putString("Action", "Delete")
                                analyticsBundle.putInt("Count", selectedFiles.count())
                                FirebaseAnalytics.getInstance(context).logEvent("WatchNotification", analyticsBundle)
                            }
                        }

                    }
                }

            }
        }
    }
}