package com.celites.yafc.kotlin.remoteconfig

import android.content.Context
import com.celites.kutils.d
import com.celites.kutils.edit
import com.celites.kutils.getDefaultSharedPreferences
import com.celites.yafc.R
import com.google.firebase.remoteconfig.FirebaseRemoteConfig

/**
 * Created by Prasham on 12/5/2016.
 */
object RemoteConfigProvider {
    val firebaseConfig: FirebaseRemoteConfig by lazy { FirebaseRemoteConfig.getInstance() }
	fun updateRemoteConfig(context: Context, afterSaved: () -> Any = {}) {
		firebaseConfig.setDefaults(R.xml.default_params)

		firebaseConfig.fetch().addOnCompleteListener {
			if (it.isSuccessful) {
				firebaseConfig.activateFetched()
				updateValues(context, afterSaved)
			} else {
				d("Remote Fetching failed reverting to default")
			}
		}
	}

	fun updateValues(context: Context, afterSaved: () -> Any = {}) {
		context.getDefaultSharedPreferences().edit {
			putLong("NO_OF_TABS", firebaseConfig.getLong("no_of_tabs"))
			putBoolean("MOVE_AVAILABLE", firebaseConfig.getBoolean("move_available"))
			putLong("ADD_BUTTON_PLACEMENT", firebaseConfig.getLong("add_button_placement"))
			putLong("MIN_SCAN_HOURS", firebaseConfig.getLong("min_scan_hours"))
			putLong("MIN_NUKE_HOURS", firebaseConfig.getLong("min_nuke_hours"))
			putLong("SETTING_PLACEMENT", firebaseConfig.getLong("setting_placement"))
			putBoolean("NOTIFICATION_VIEW_AVAILABLE", firebaseConfig.getBoolean("notification_view_available"))
			putBoolean("FAQ_CAN_ASK_QUESTIONS", firebaseConfig.getBoolean("faq_can_ask_questions"))
			putBoolean("BETA_AVAILABLE", firebaseConfig.getBoolean("beta_available"))
			putString("ASK_QUESTION_KEY", firebaseConfig.getString("ask_question_key"))
			afterSaved()
		}
	}
}