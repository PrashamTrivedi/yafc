package com.celites.yafc.kotlin.files

import android.content.Context
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import android.support.v4.provider.DocumentFile
import com.celites.kutils.containsInArray
import com.celites.kutils.d
import com.celites.kutils.edit
import com.celites.kutils.getDefaultSharedPreferences
import com.celites.kutils.removeNotification
import com.celites.kutils.setNotification
import com.celites.kutils.v
import com.celites.kutils.w
import com.celites.yafc.R
import com.celites.yafc.kotlin.Utils.getFilesFromPath
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crash.FirebaseCrash
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription

/**
 * Created by Prasham on 9/17/2016.
 */
fun String.deleteFromPath(name: String, id: Int = 0, context: Context, selectedFiles: Array<out String> = emptyArray()) {
    this.getFilesFromPath(context)
            .filter {
                selectedFiles.isEmpty() || (it.name.containsInArray(*selectedFiles))
            }
            .map { context to it }
            .compose { it.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()) }.
            subscribe(object : Subscriber<Pair<Context, DocumentFile>> {
                override fun onSubscribe(s: Subscription?) {

                }

                var index = 0
                var localContext: Context? = null
                override fun onComplete() {
                    if (localContext == null) {
                        localContext = context
                    }
                    localContext?.let {
                        it.setNotification(id, {
                            v("Work done successfully")
                            setContentTitle(name)
                            setGroup("Delete")
                            setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                            setAutoCancel(true)
                            setSmallIcon(R.drawable.ic_delete_black_24dp)
                            val message = if (index > 0) "Congrats! $index files deleted." else "No files deleted"
                            d("message is $message")
                            setContentText(message)
                            val bigTextStyle = NotificationCompat.BigTextStyle(this)
                            bigTextStyle.setBigContentTitle(name)
                            bigTextStyle.bigText(message)
                            setStyle(bigTextStyle)
                            val defaultSharedPreferences = context.getDefaultSharedPreferences()
                            val totalDeleted = defaultSharedPreferences.getInt("group_total_deleted", 0);
                            defaultSharedPreferences.edit {
                                putInt("group_total_deleted", index + totalDeleted)
                            }

                        })
                        it.setNotification(29569, {
                            setContentTitle(name)
                            setGroup("Delete")
                            setGroupSummary(true)
                            setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                            setAutoCancel(true)
                            setSmallIcon(R.drawable.ic_delete_black_24dp)
                            val defaultSharedPreferences = context.getDefaultSharedPreferences()
                            val totalDeleted = defaultSharedPreferences.getInt("group_total_deleted", 0);
                            val message = if (totalDeleted > 0) "Congrats! $totalDeleted files deleted." else "No files deleted"
                            d("message is $message")
                            setContentText(message)
                            val bigTextStyle = NotificationCompat.BigTextStyle(this)
                            bigTextStyle.setBigContentTitle(name)
                            bigTextStyle.bigText(message)
                            setStyle(bigTextStyle)
                        })
                    }

                    if (index > 0) {
                        val analyticsBundle = Bundle()
                        analyticsBundle.putString("Action", "Delete")
                        analyticsBundle.putInt("Count", index)
                        FirebaseAnalytics.getInstance(context).logEvent("Deletion", analyticsBundle)
                    }
                }

                override fun onNext(pair: Pair<Context, DocumentFile>) {
                    localContext = pair.first
                    val file = pair.second
                    val fileName = file.name
                    if (file.exists() && file.delete()) {
                        index += 1
                        v("Deleting $fileName")
                        localContext?.let {
                            it.setNotification(id, {
                                setContentTitle(name)
                                setAutoCancel(true)
                                setGroup("Delete")
                                setSmallIcon(R.drawable.ic_delete_black_24dp)
                                setContentText("Deleting $fileName")
                                setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                                val bigTextStyle = NotificationCompat.BigTextStyle(this)
                                bigTextStyle.setBigContentTitle(name)
                                bigTextStyle.bigText("Deleting $fileName")
                                setStyle(bigTextStyle)
                            })
                        }


                    }

                }

                override fun onError(e: Throwable?) {
                    e?.let {
                        w("Error", it.toString())
                        FirebaseCrash.report(it)

                    }
                    context.removeNotification(id)

                }
            })
}

