package com.celites.yafc.kotlin.home.adapters

import android.content.Context
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.view.View
import com.celites.yafc.R
import com.celites.yafc.kotlin.home.fragments.WatchedFileFragment
import com.celites.yafc.kotlin.home.models.TabModel

/**
 * Created by Prasham on 2/21/2016.
 */
class PageAdapter(var context: Context, var fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {

    var tabsList: MutableList<TabModel> = arrayListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    var bundle: Bundle? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

	interface FileAddRequestClickListener {
		fun onFileAddRequestClick(view: View?)
	}

    var onDirectoryClickListener: GridAdapter.OnDirectoryClickListener? = null
        set(value) {
            field = value
        }
    var fileAddRequestClickListener: FileAddRequestClickListener? = null
        set(value) {
            field = value
        }

	interface OnDeleteFinishedListener {
		fun onDeleted(index: Int)
	}

	interface OnFileSelectionListener {
		fun onFilesSelected(numberOfSelectedFiles: Int)
	}

    var onDeletedClickListener: OnDeleteFinishedListener? = null
        set(value) {
            field = value
        }

    var onFileSelectionListener: OnFileSelectionListener? = null
        set(value) {
            field = value
        }

	fun addTab(tabModel: TabModel) {

		tabsList.add(tabModel)
		notifyDataSetChanged()
	}

    override fun getItemPosition(`object`: Any?): Int {
        return POSITION_NONE
    }


    fun getTab(position: Int) = tabsList.get(position)
    override fun getCount(): Int = tabsList.size

    override fun getItem(p0: Int): WatchedFileFragment? {
        val localBundle = Bundle()
        localBundle.putString(context.getString(R.string.bundle_file_path), getTab(p0).path)
        localBundle.putString(context.getString(R.string.bundle_dir_name), getTab(p0).name)

        bundle?.let {
            localBundle.putAll(bundle)
        }

        val fragment = WatchedFileFragment()
        fragment.fileAddRequestClickListener = fileAddRequestClickListener
        fragment.onDeleteFinishedListener = onDeletedClickListener
        fragment.onFileSelectionListener = onFileSelectionListener
        fragment.onDirectoryClickListener = onDirectoryClickListener
        fragment.arguments = localBundle
        return fragment

    }

    override fun getPageTitle(position: Int): CharSequence? {
        return getTab(position).name
    }

}