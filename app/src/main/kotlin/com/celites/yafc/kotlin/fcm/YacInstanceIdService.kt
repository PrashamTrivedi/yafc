package com.celites.yafc.kotlin.fcm

import com.celites.yafc.kotlin.auth.AuthProvider
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService


/**
 * Created by Prash on 02-01-2017.
 */
class YacInstanceIdService : FirebaseInstanceIdService() {
    override fun onTokenRefresh() {
        super.onTokenRefresh()
        val refreshedToken = FirebaseInstanceId.getInstance().token
        AuthProvider.registerToken(refreshedToken)
    }
}