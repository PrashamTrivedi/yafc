package com.celites.yafc.kotlin.faq.fragments

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewAnimationUtils
import android.view.ViewGroup
import com.celites.kutils.getDefaultSharedPreferences
import com.celites.kutils.isEmptyString
import com.celites.yafc.R
import com.celites.yafc.kotlin.auth.AuthProvider
import com.celites.yafc.kotlin.data.FirebaseDb
import kotlinx.android.synthetic.main.layout_ask_question.*
import java.util.*


/**
 * Created by Prash on 18-01-2017.
 */
class AskQuestionFragment : DialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.layout_ask_question, container, false)
    }


    override fun onStart() {
        super.onStart()
        dialog?.let {
            it.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            it.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }


    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view?.viewTreeObserver?.addOnGlobalLayoutListener {
            val cx = view.width / 2
            val cy = view.height / 2
            val radius = Math.hypot(cx.toDouble(), cy.toDouble()).toFloat()
            val anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0f, radius)
            anim.start()
        }
        askQuestionButton.setOnClickListener {
            if (!askQuestion.text.isEmptyString()) {
                val child = FirebaseDb.databaseReference.child(context.getDefaultSharedPreferences().getString("ASK_QUESTION_KEY", "faqs/userQuestions"))
                val userData = HashMap<String, String>()
                userData.put("question", askQuestion.text.toString())
                AuthProvider.currentUser?.let {
                    val quechild = child.child(it.uid)
                    quechild.setValue(userData)
                    quechild.push()
                    dismiss()
                }

            } else {
                askQuestionParent.error = "Please enter your question"
            }

        }
    }
}
