package com.celites.yafc.kotlin.data

import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

/**
 * Created by Prasham on 11/22/2016.
 */
object FirebaseDb {
	val databaseReference by lazy {
		database.reference
	}

	val database by lazy {
		FirebaseDatabase.getInstance()
	}


	val faqQuestions by lazy {
		databaseReference.child("faqs").child("questions")
	}

	val userProperties by lazy {
		databaseReference.child("userProperties")
	}


	fun DatabaseReference.getOfflineReference() = this.keepSynced(true)


}