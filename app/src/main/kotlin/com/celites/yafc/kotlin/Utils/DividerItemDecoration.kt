package com.celites.yafc.kotlin.Utils

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by Prasham on 8/15/2016.
 */
class DividerItemDecoration(var spaces: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect?, view: View?, parent: RecyclerView?, state: RecyclerView.State?) {
        outRect?.let {
            it.left = spaces
            it.right = spaces
            it.bottom = spaces
            if (parent?.getChildAdapterPosition(view) == 0)
                it.top = spaces;
        }
    }
}