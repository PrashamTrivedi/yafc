package com.celites.yafc.kotlin.home.adapters

import android.content.Context
import android.support.v4.provider.DocumentFile
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import com.bumptech.glide.RequestManager
import com.celites.kutils.d
import com.celites.kutils.setVisible
import com.celites.yafc.R
import com.celites.yafc.kotlin.Utils.KotlinStorageAccessFileWriter
import com.celites.yafc.kotlin.Utils.setUrlWithGlide
import com.celites.yafc.kotlin.home.viewmodels.FileViewModel
import kotlinx.android.synthetic.main.row_file_detail.view.*
import java.util.*

/**
 * Created by Prasham on 3/21/2016.
 */
class GridAdapter(var context: Context, var glide: RequestManager) : RecyclerView.Adapter<GridAdapter.GridViewHolder>() {

    lateinit var layoutInflater: LayoutInflater

    val TAG = "GridAdapter"

    var filesList: MutableList<FileViewModel> = arrayListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

	interface AfterLongClickListener {
		fun afterLongClickListener()
	}

	interface OnDirectoryClickListener {
		fun onDirectoryClick(documentFile: DocumentFile?)
	}


	fun addFile(file: FileViewModel) {
		filesList.add(file)
		notifyItemInserted(filesList.size)
	}


    var afterLongClickListener: AfterLongClickListener? = null
        set(value) {
            field = value
        }
    var onDirectoryClickListener: OnDirectoryClickListener? = null
        set(value) {
            field = value
        }

    init {
        layoutInflater = LayoutInflater.from(context)
    }

    fun getItem(position: Int) = filesList[position]
    override fun getItemCount() = filesList.size


    override fun onBindViewHolder(holder: GridViewHolder?, position: Int) {
        val file = getItem(position)

        holder?.setFileModel(file)
        holder?.itemView?.fileName?.tag = file to position
        holder?.itemView?.tag = file
        holder?.itemView?.setOnClickListener {
            val fileModel = it.tag as FileViewModel
            val innerFile = fileModel.fileModel
            if (innerFile.documentFile?.isDirectory ?: false) {
                onDirectoryClickListener?.onDirectoryClick(innerFile.documentFile)
            } else {
                fileModel.openFile()
            }
        }
        holder?.itemView?.fileName?.setOnClickListener {
            val modelIntPair = it.tag as kotlin.Pair<FileViewModel, Int>
            val fileViewModelTag = modelIntPair.first
            if (it is CheckBox) {
                fileViewModelTag.isSelected = it.isChecked
            }
            val positionOfFile = modelIntPair.second
            filesList[positionOfFile] = fileViewModelTag
            afterLongClickListener?.afterLongClickListener()
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): GridViewHolder? {
        return GridViewHolder(layoutInflater.inflate(R.layout.row_file_detail, parent, false), glide)
    }

	val selectedCounts: Int
		get() = filesList.count { it.isSelected }

    fun clearSelection() {
        filesList.map { it.isSelected = false }
        notifyDataSetChanged()
    }

    fun getSelectedObjects() = filesList.filter { it.isSelected }

    fun deleteSelected(afterDeleted: () -> Any = {}, errorInDeletion: () -> Any = {}) {
        deleteFilesInList(afterDeleted, errorInDeletion)
    }

    private fun deleteFilesInList(afterDeleted: () -> Any, errorInDeletion: () -> Any, isSelected: Boolean = true) {
        val indexList: MutableList<Int> = arrayListOf()


        for ((index, file) in filesList.withIndex().filter { it.value.isSelected == isSelected }) {
            d(TAG, "Deleting File ${file.name} At Index ${index}")
            file.delete({ indexList.add(index) }, { errorInDeletion })
        }

        filesList.removeAll { it.isDeleted }
        Collections.sort(indexList)
        Collections.reverse(indexList)

        for (index in indexList) {
            notifyItemRemoved(index)
        }

        afterDeleted()
    }

	fun moveFilesInList(afterDeleted: () -> Any, errorInDeletion: () -> Any, documentFile: DocumentFile, safWriter: KotlinStorageAccessFileWriter) {
		val indexList: MutableList<Int> = arrayListOf()


		for ((index, file) in filesList.withIndex().filter { it.value.isSelected }) {
			d(TAG, "Deleting File ${file.name} At Index ${index}")
			file.moveToPath(safWriter, documentFile, { indexList.add(index) }, { errorInDeletion })
		}

		filesList.removeAll { it.isDeleted }
		Collections.sort(indexList)
		Collections.reverse(indexList)

		for (index in indexList) {
			notifyItemRemoved(index)
		}

		afterDeleted()
	}

    fun deleteNotSelected(afterDeleted: () -> Any = {}, errorInDeletion: () -> Any = {}) {

        deleteFilesInList(afterDeleted, errorInDeletion, false)
    }


    class GridViewHolder(var view: View, var glide: RequestManager) : RecyclerView.ViewHolder(view) {

        fun setFileModel(fileViewModel: FileViewModel) {
            itemView.imagePreview.setUrlWithGlide(fileViewModel.uri, glide, fileViewModel.fallbackUri)

            itemView.fileName.text = fileViewModel.name
            itemView.fileName.isChecked = fileViewModel.isSelected
            itemView.memorySize.text = fileViewModel.size
            itemView.createdDate.text = fileViewModel.createdDate


            itemView.fileName.isEnabled = !fileViewModel.isDirectory
            itemView.memorySize.setVisible(!fileViewModel.isDirectory)
            itemView.createdDate.setVisible(!fileViewModel.isDirectory)
        }
    }

}
