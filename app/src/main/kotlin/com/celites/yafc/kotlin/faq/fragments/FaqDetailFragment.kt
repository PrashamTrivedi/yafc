package com.celites.yafc.kotlin.faq.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.celites.yafc.R
import com.celites.yafc.kotlin.Utils.setSupportActionbar
import com.celites.yafc.kotlin.faq.models.Faqs
import kotlinx.android.synthetic.main.fragment_question_answer.*

/**
 * Created by Prasham on 11/24/2016.
 */
class FaqDetailFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_question_answer, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        faq?.let {
            faqDetailToolbar.title = it.question
            setSupportActionbar(faqDetailToolbar)
            questionText.text = it.question
            answerText.text = it.answer
        }
    }

    var faq: Faqs? = null
        set(value) {
            field = value
            update()
        }

    private fun update() {

    }
}