package com.celites.yafc.kotlin.faq.viewmodels

import android.content.Context
import com.celites.yafc.kotlin.faq.adapters.FaqsAdapter
import com.celites.yafc.kotlin.faq.models.Faqs

/**
 * Created by Prasham on 11/24/2016.
 */
class FaqViewModel(val faq: Faqs, val context: Context) {
	fun getQuestion() = faq.question
	fun getAnswer() = faq.answer
	fun openQuestion(onFaqOpenListener: FaqsAdapter.OnFaqOpenListener?) {

		onFaqOpenListener?.onFaqOpen(faq)
	}
}