package com.celites.yafc.kotlin.home.viewmodels

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v4.provider.DocumentFile
import android.text.format.DateUtils
import android.text.format.Formatter
import com.celites.yafc.kotlin.Utils.KotlinStorageAccessFileWriter
import com.celites.yafc.kotlin.home.models.FileModel

/**
 * Created by Prasham on 3/18/2016.
 */
class FileViewModel(val context: Context, val fileModel: FileModel, val hasPreview: Boolean = false) {


	val uri: Uri
		get() = Uri.parse(fileModel.previewUrl)

	val fallbackUri: Int
		get() = fileModel.iconId

	val isDirectory: Boolean
		get() = fileModel.documentFile?.isDirectory ?: false

	val name: String
		get() = fileModel.name

    var isSelected: Boolean = false
        set(value) {
            field = value
        }

    var isDeleted: Boolean = false
        set(value) {
            field = value
        }

	fun openFile() {

		fileModel.documentFile?.let {
			val intent = Intent(Intent.ACTION_VIEW)
			intent.data = it.uri
			intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
			context.startActivity(intent)
		}
	}

	val size: String
		get() = Formatter.formatShortFileSize(context, fileModel.size) ?: "Size unavailable"

	fun delete(afterDeleted: () -> Any = {}, errorInDeletion: () -> Any = {}) {
		val deleted = fileModel.documentFile?.delete() ?: false
		isDeleted = deleted
		if (deleted) {
			afterDeleted()
		} else {
			errorInDeletion()
		}
	}

    fun moveToPath(safWriter: KotlinStorageAccessFileWriter, documentFile: DocumentFile, afterDeleted: () -> Any = {}, errorInDeletion: () -> Any =
    {}) {
        fileModel.documentFile?.let {
            val moveFile = safWriter.moveFile(it, documentFile)
            if (moveFile) {
                afterDeleted()
            } else {
                errorInDeletion()
            }
        }

    }


    val createdDate: CharSequence?
        get() = DateUtils.getRelativeTimeSpanString(fileModel.documentFile?.lastModified() ?: 0, System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS, DateUtils.FORMAT_ABBREV_ALL)

}

