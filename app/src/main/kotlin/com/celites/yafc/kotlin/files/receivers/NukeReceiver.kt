package com.celites.yafc.kotlin.files.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.celites.kutils.d
import com.celites.kutils.isEmptyString
import com.celites.kutils.removeNotification
import com.celites.kutils.v
import com.celites.yafc.R
import com.celites.yafc.kotlin.data.DataRepository
import com.celites.yafc.kotlin.files.deleteFromPath
import com.google.firebase.analytics.FirebaseAnalytics

/**
 * Created by Prasham on 9/9/2016.
 */
class NukeReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val action = intent?.action ?: ""
        d("NukeReceiver", "Action $action")

        if (!action.isEmptyString()) {
            context?.let {
                val path = intent?.extras?.getString(it.getString(R.string.bundle_selected_path)) ?: ""
                val selectedFiles = intent?.extras?.getStringArray(it.getString(R.string.bundle_selected_files)) ?: emptyArray()
                val dataRepo = DataRepository().initWith(it)
                val tabModel = dataRepo.getTabByPath(path)
                tabModel?.let {
                    v("NukeReceiver", "Awoken to $action for $path")
                    if (!path.isEmptyString() && action.equals(context.getString(R.string.action_delete), true)) {
                        path.deleteFromPath(tabModel.name, tabModel.id, context, selectedFiles)
                        val analyticsBundle = Bundle()
                        analyticsBundle.putString("Action", "Delete")
                        analyticsBundle.putInt("Count", selectedFiles.count())
                        FirebaseAnalytics.getInstance(context).logEvent("NukeNotification", analyticsBundle)
                    } else if (action.equals(context.getString(R.string.bundle_action_not_now), true)) {
                        context.removeNotification(tabModel.id)
                    }
                }
            }

        }
    }
}