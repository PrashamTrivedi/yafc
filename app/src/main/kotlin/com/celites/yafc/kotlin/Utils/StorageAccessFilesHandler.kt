package com.celites.yafc.kotlin.Utils

import android.content.ContentResolver
import android.content.ContentUris
import android.content.Context
import android.net.Uri
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.support.v4.provider.DocumentFile
import com.celites.kutils.getDataColumn
import com.celites.kutils.isAudio
import com.celites.kutils.isEmptyString
import com.celites.kutils.isImage
import com.celites.kutils.isVideo
import java.io.File

/**
 * Created by Prasham on 1/2/2016.
 */
fun String.getExtension(): String {
    if (isEmptyString()) {
        return ""
    } else {
        var dot = lastIndexOf(".")
        if (dot >= 0) {
            return substring(dot)
        } else {
            return ""
        }
    }
}

fun String.isLocal() = !isEmptyString() && (startsWith("http://") || startsWith("https://"))

fun Uri.isMediaUri() = authority.equals("media", true)

fun File.getUri() = Uri.fromFile(this)

fun Uri.isExternalStorageDocument() = authority.equals("com.android.externalstorage.documents")

fun Uri.isDownloadDocuments() = authority.equals("com.android.providers.downloads.documents")

fun Uri.isMediaDocument() = authority.equals("com.android.providers.media.documents")

fun DocumentFile.getFilePath(context: Context): Uri {
    var path = ""
    val id = DocumentsContract.getDocumentId(this.uri)

    val split = id.split(":")
    val mediaType = split[0]
    val selection = "_id=?"
    val selectionArg = arrayOf(context.getFilePath(uri))
	if (isAudio) {
        path = context.getDataColumn(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, selection, selectionArg)
	} else if (isVideo) {
        path = context.getDataColumn(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, selection, selectionArg)
	} else if (isImage) {
        path = context.getDataColumn(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, selection, selectionArg)
    } else if (uri.isMediaUri()) {
        path = context.getDataColumn(uri, null, null)
    } else if (uri.isExternalStorageDocument()) {
        val docId = DocumentsContract.getDocumentId(uri)
        val split = docId.split(":")
        val storageType = split[0]

        if (storageType.equals("primary", true)) {
            path = Environment.getExternalStorageDirectory().path + "/" + split[1];
        }
    } else if (uri.isDownloadDocuments()) {
        val id = DocumentsContract.getDocumentId(uri)
        val contentUri = ContentUris.withAppendedId(
                Uri.parse("content://downloads/public_downloads"),
                id.toLong());
        path = context.getDataColumn(contentUri, null, null)
    }
    if (!path.startsWith("file://")) {
        path = "file://".plus(path)
    }
    var file = File(path)


    return Uri.fromFile(file)
}


fun Context.getFilePath(uri: Uri): String {
    var path = ""
    if (uri.isExternalStorageDocument()) {
        val docId = DocumentsContract.getDocumentId(uri)
        val split = docId.split(":")
        val storageType = split[0]

        if (storageType.equals("primary", true)) {
            path = Environment.getExternalStorageDirectory().path + "/" + split[1];
        }
    } else if (uri.isDownloadDocuments()) {
        val id = DocumentsContract.getDocumentId(uri)
        val contentUri = ContentUris.withAppendedId(
                Uri.parse("content://downloads/public_downloads"),
                id.toLong());
        path = getDataColumn(contentUri, null, null)
    } else if (uri.isMediaDocument()) {
        val id = DocumentsContract.getDocumentId(uri)
        val split = id.split(":")
        val mediaType = split[0]
        val selection = "_id=?"
        val selectionArg = arrayOf(split[1])

        if (mediaType.equals("image", true)) {
            path = getDataColumn(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, selection, selectionArg)
        } else if (mediaType.equals("video", true)) {
            path = getDataColumn(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, selection, selectionArg)
        } else if (mediaType.equals("audio", true)) {
            path = getDataColumn(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, selection, selectionArg)
        }


    } else if (uri.isMediaUri()) {
        path = getDataColumn(uri, null, null)
    }
    return path
}

fun ContentResolver.getRealPathFromUri(contentUri: Uri): String {
    val proj = arrayOf(MediaStore.Audio.Media.DATA)
    val cursor = query(contentUri, proj, null, null, null)
    try {
        var columnIndex = 0
        if (cursor != null) {
            columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA)
            cursor.moveToFirst()
            return cursor.getString(columnIndex)
        } else {
            return ""
        }
    } finally {
        if (cursor != null) {
            cursor.close()
        }

    }
}

fun Context.getDataColumn(uri: Uri, selection: String?, selectionArg: Array<String>?): String {
    val column = "_data"
    val projection = arrayOf(column)

    val cursor = contentResolver.query(uri, projection, selection, selectionArg, null)
    try {
        if (cursor != null) {
            val columnIndex = cursor.getColumnIndexOrThrow(column)
            cursor.moveToFirst()
            return cursor.getString(columnIndex)
        }
    } finally {
        if (cursor != null)
            cursor.close();

    }
    return ""
}