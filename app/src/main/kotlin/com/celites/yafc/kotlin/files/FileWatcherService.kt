package com.celites.yafc.kotlin.files

import android.app.PendingIntent
import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.support.v4.app.TaskStackBuilder
import android.support.v4.content.ContextCompat
import com.celites.kutils.d
import com.celites.kutils.edit
import com.celites.kutils.getDefaultSharedPreferences
import com.celites.kutils.isEmptyString
import com.celites.kutils.setNotification
import com.celites.kutils.v
import com.celites.kutils.w
import com.celites.yafc.R
import com.celites.yafc.kotlin.Utils.getFilesFromPath
import com.celites.yafc.kotlin.data.DataRepository
import com.celites.yafc.kotlin.files.receivers.DeleteReceiver
import com.celites.yafc.kotlin.files.receivers.WatchReceiver
import com.celites.yafc.kotlin.home.MainActivity
import com.celites.yafc.kotlin.home.models.TabModel
import com.google.firebase.crash.FirebaseCrash
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription

class FileWatcherService : JobService() {
	override fun onStopJob(params: JobParameters?): Boolean {
		return true
	}

	override fun onStartJob(params: JobParameters?): Boolean {
		val path = params?.extras?.getString(getString(R.string.bundle_file_path)) ?: "Service"
		val name = params?.extras?.getString(getString(R.string.bundle_dir_name)) ?: "Nemo"

		val dataRepo = DataRepository().initWith(this)
		val tabModel = dataRepo.getTabByPath(path)


		val lastTime = this@FileWatcherService.getDefaultSharedPreferences().getLong("COMPARE_TIME_IN_$name", 0L)
		val startTime = System.currentTimeMillis()
		d("Started service for $name at ${System.currentTimeMillis()} ")
		path.getFilesFromPath(this@FileWatcherService).filter {
			lastTime == 0L || it.lastModified() > lastTime
		}.toSortedList { fileModelLhs, fileModelRhs ->
			val lastModifiedLhs = fileModelLhs?.lastModified() ?: 0L
			val lastModifiedRhs = fileModelRhs?.lastModified() ?: 0L
			lastModifiedLhs.compareTo(lastModifiedRhs)
		}.flattenAsFlowable { it }.map { it.name }.compose {
			it.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
		}.doOnTerminate { jobFinished(params, false) }.subscribe(object : Subscriber<String> {
			override fun onSubscribe(s: Subscription?) {
				s?.request(Long.MAX_VALUE)
			}

			var strings: MutableList<String> = arrayListOf()
			override fun onError(e: Throwable?) {
				e?.let {
					w("Error", it.toString())
					FirebaseCrash.report(it)

				}
				val firstMessage = "Error while Scanning of $name"
				val secondMessage = "."
				val message = "$firstMessage$secondMessage"
				notifyWithMessage(path = path, name = name, message = message, tabModel = tabModel)
			}

			override fun onNext(t: String?) {
				t?.let {
					if (!it.isEmptyString()) {
						strings.add(it)
					}
				}
			}

			override fun onComplete() {
				val count = strings.count()
				d("Completed at ${System.currentTimeMillis()}, with count = $count")
				tabModel?.let {

					if (count > 0) {
						val secondMessage = "$count new items are found, Click here to see them"

						val bundle = Bundle()
						bundle.putString(getString(R.string.bundle_selected_path), path)
						bundle.putBoolean(getString(R.string.bundle_generate_notifications), false)

						bundle.putStringArray(getString(R.string.bundle_selected_files), strings.toTypedArray())
						notifyWithMessage(path, name, secondMessage, strings, true, bundle, tabModel)
						getDefaultSharedPreferences().edit {
							putString("new_group_${path}", "$name: ${strings.size} new files")
						}
						updateGroupNotification(name)
					}
				}
			}

		})


		return true
	}

	private fun notifyWithMessage(path: String, name: String, message: String, strings: MutableList<String>? = null, allowActions: Boolean = false,
	                              bundle: Bundle? = null, tabModel: TabModel?) {
		val id = tabModel?.id ?: 0
		setNotification(id, {
			val fileWatcherService = this@FileWatcherService
			setContentTitle(name)
			setColor(ContextCompat.getColor(fileWatcherService, R.color.colorPrimary))
			setGroup("Watch")
			setSortKey(id.toString())
			setAutoCancel(true)

			setContentText(message)
			setSmallIcon(R.drawable.ic_action_watch)
			if (getDefaultSharedPreferences().getBoolean("NOTIFICATION_VIEW_AVAILABLE", false)) {
				strings?.let {
					for (string in strings.toTypedArray()) {
						v("Checking string $string")
					}
				}
				val pendingIntent = getIntent(fileWatcherService, bundle, requestCode = id)
				setContentIntent(pendingIntent)
			}

			setSubText("Click to see the files")

			if (strings == null || strings.isEmpty()) {
				val bigTextStyle = NotificationCompat.BigTextStyle(this)
				bigTextStyle.bigText(message)
				setStyle(bigTextStyle)
			} else {
				val inboxStyle = NotificationCompat.InboxStyle(this)

				for (s in strings) {
					inboxStyle.addLine(s)
				}
				inboxStyle.setBigContentTitle(name)
				inboxStyle.setSummaryText(message)
				setStyle(inboxStyle)
			}
			val intent = Intent(this@FileWatcherService, DeleteReceiver::class.java)
			val deleteIntent = PendingIntent.getBroadcast(this@FileWatcherService, id, intent, PendingIntent.FLAG_CANCEL_CURRENT)


			setDeleteIntent(deleteIntent)
			if (allowActions) {
				bundle?.let {
					val deleteBundle = Bundle()
					deleteBundle.putString(getString(R.string.bundle_selected_path), path)
					strings?.let {
						val stringsArray = it.toTypedArray()
						for (string in stringsArray) {
							v("Checking string $string")
						}
						deleteBundle.putStringArray(getString(R.string.bundle_selected_files), stringsArray)
						deleteBundle.putString(getString(R.string.bundle_selection_type), getString(R.string.bundle_selection_all_files))
						deleteBundle.putString(getString(R.string.bundle_action), getString(R.string.bundle_action_delete))

						val deleteActionIntent = Intent(this@FileWatcherService, WatchReceiver::class.java)
						deleteActionIntent.putExtras(deleteBundle)
						deleteActionIntent.action = getString(R.string.bundle_action_delete)

						val deleteIntent = PendingIntent.getBroadcast(this@FileWatcherService, id, deleteActionIntent,
						                                              PendingIntent.FLAG_UPDATE_CURRENT)
						addAction(R.drawable.ic_delete_black_24dp, getString(R.string.action_delete), deleteIntent)

						val externalDirUrl = this@FileWatcherService.getDefaultSharedPreferences().getString("APP_EXTERNAL_PARENT_FILE_URI", "")
						if (!externalDirUrl.isEmptyString()) {
							val moveBundle = Bundle()

							moveBundle.putString(getString(R.string.bundle_selected_path), path)
							moveBundle.putBoolean(getString(R.string.bundle_generate_notifications), false)
							moveBundle.putStringArray(getString(R.string.bundle_selected_files), stringsArray)
							moveBundle.putString(getString(R.string.bundle_selection_type), getString(R.string.bundle_selection_all_files))
							moveBundle.putString(getString(R.string.bundle_action), getString(R.string.bundle_action_not_now))

							val moveIntent = getIntent(fileWatcherService, moveBundle, getString(R.string.bundle_action_move), id)

							addAction(R.drawable.ic_content_cut_black_24dp, getString(R.string.action_move), moveIntent)
						}
					}
				}


			}

		})
	}

	private fun updateGroupNotification(name: String) {
		val pathKeys = getDefaultSharedPreferences().all.filterKeys { it.startsWith("new_group_") }
		setNotification(24888, {
			setContentTitle("New Files")
			setSmallIcon(R.drawable.ic_action_watch)
			setColor(ContextCompat.getColor(this@FileWatcherService, R.color.colorPrimary))
			setGroup("Watch")
			setGroupSummary(true)
			setContentText("New Files are added")
			val pendingIntent = getIntent(this@FileWatcherService, requestCode = 24888)
			setContentIntent(pendingIntent)

			if (pathKeys.isEmpty()) {
				val bigTextStyle = NotificationCompat.BigTextStyle(this)
				bigTextStyle.bigText("New Files are added")
				setStyle(bigTextStyle)
			} else {
				val inboxStyle = NotificationCompat.InboxStyle(this)

				for ((key, value) in pathKeys) {
					inboxStyle.addLine(value.toString())
				}
				inboxStyle.setBigContentTitle(name)
				inboxStyle.setSummaryText("New Files are Added")
				setStyle(inboxStyle)
			}

		})
	}


	private fun getIntent(fileWatcherService: FileWatcherService, bundle: Bundle? = null, action: String = "View",
	                      requestCode: Int = 0): PendingIntent? {
		val intent = Intent(fileWatcherService, MainActivity::class.java)
		bundle?.let {
			intent.putExtras(it)
		}
		intent.action = action
		val stackBuilder: TaskStackBuilder = TaskStackBuilder.create(fileWatcherService)
		stackBuilder.addNextIntent(intent)
		val pendingIntent = stackBuilder.getPendingIntent(requestCode, PendingIntent.FLAG_UPDATE_CURRENT)
		return pendingIntent
	}
}
