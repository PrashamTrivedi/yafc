package com.celites.yafc.kotlin.faq.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.celites.yafc.R
import com.celites.yafc.kotlin.faq.models.Faqs
import com.celites.yafc.kotlin.faq.viewmodels.FaqViewModel
import kotlinx.android.synthetic.main.row_file_detail.view.*
import kotlinx.android.synthetic.main.row_question.view.*

/**
 * Created by Prasham on 3/21/2016.
 */
class FaqsAdapter(var context: Context) : RecyclerView.Adapter<FaqsAdapter.FaqsViewHolder>() {

    var layoutInflater: LayoutInflater = LayoutInflater.from(context)

    val TAG = "GridAdapter"

    interface OnFaqOpenListener {
        fun onFaqOpen(faqs: Faqs)
    }

    var onFaqOpenListener: OnFaqOpenListener? = null
        set(value) {
            field = value
        }
    var faqsList: MutableList<FaqViewModel> = arrayListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    fun getItem(position: Int) = faqsList[position]
    override fun getItemCount() = faqsList.size


    override fun onBindViewHolder(holder: FaqsViewHolder?, position: Int) {
        val faqs = getItem(position)

        holder?.setFaqModel(faqs)
        holder?.itemView?.fileName?.tag = faqs to position
        holder?.itemView?.tag = faqs
        holder?.itemView?.setOnClickListener {
            val fileModel = it.tag as FaqViewModel
            fileModel.openQuestion(onFaqOpenListener)
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): FaqsViewHolder? {
        return FaqsViewHolder(layoutInflater.inflate(R.layout.row_question, parent, false))
    }


    class FaqsViewHolder(var view: View) : RecyclerView.ViewHolder(view) {

        fun setFaqModel(faqViewModel: FaqViewModel) {
            itemView.questionText.text = faqViewModel.getQuestion()

        }

    }
}
