package com.celites.yafc.kotlin.files

import android.app.job.JobParameters
import android.app.job.JobService
import com.celites.yafc.R
import com.celites.yafc.kotlin.Utils.KotlinStorageAccessFileWriter
import com.celites.yafc.kotlin.Utils.getDocumentTree
import com.celites.yafc.kotlin.data.DataRepository

/**
 * Created by Prasham on 9/30/2016.
 */
class DebugWatcherService : JobService() {
    override fun onStartJob(params: JobParameters?): Boolean {
        val path = params?.extras?.getString(getString(R.string.bundle_file_path)) ?: "Service"

        val dataRepo = DataRepository().initWith(this)
        val tabModel = dataRepo.getTabByPath(path)

        tabModel.let {
            val safWriter = KotlinStorageAccessFileWriter(1212)
            safWriter.startWithContext(this)
            tabModel?.path.let {
                val documentTree = it?.getDocumentTree(this@DebugWatcherService)
                try {
                    if (documentTree?.exists() ?: false) {
                        documentTree?.parentFile?.createDirectory(tabModel?.name)
                    }
                    documentTree?.let { safWriter.writeDataToTimeStampedFile("txt", "Test", "txt", false, documentTree) }
                } catch(e: Exception) {
                    e.printStackTrace()
                }

            }
        }

        jobFinished(params, false)


        return true
    }

    override fun onStopJob(params: JobParameters?): Boolean {

        return true
    }

}