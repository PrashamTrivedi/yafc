package com.celites.yafc.kotlin.home.models


/**
 * Created by Prasham on 10/24/2016.
 */
data class TabMetadata(var tabModel: TabModel, var noOfFiles: Int = 0, var noOfDirectories: Int = 0, var size: String = "Unknown")