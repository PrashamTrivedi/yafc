package com.celites.yafc.kotlin.auth

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import com.celites.kutils.isEmptyString
import com.celites.kutils.v
import com.celites.yafc.kotlin.auth.models.UserInfoModel
import com.celites.yafc.kotlin.data.FirebaseDb
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import java.util.*

/**
 * Created by Prasham on 11/12/2016.
 */
object AuthProvider : LifecycleObserver {

	var lifeCycle: Lifecycle? = null
		set(value) {
			field = value
			field?.addObserver(this)
		}
	val firebaseAuth by lazy { FirebaseAuth.getInstance() }

	var currentUser: FirebaseUser? = null

	var authStateListener = FirebaseAuth.AuthStateListener {
		it.currentUser?.let {
			currentUser = it
		}
	}


	@OnLifecycleEvent(Lifecycle.Event.ON_START) fun addAuthListener() = firebaseAuth.addAuthStateListener(authStateListener)

	@OnLifecycleEvent(Lifecycle.Event.ON_STOP) fun removeAuthListener() {
		firebaseAuth.removeAuthStateListener(authStateListener)
		lifeCycle?.removeObserver(this)
	}


	fun signInAnonymously(onCompleted: () -> Any = {}) {
		firebaseAuth.signInAnonymously().addOnCompleteListener { task ->
			if (lifeCycle?.currentState?.isAtLeast(Lifecycle.State.CREATED) ?: false) {
				if (task.isSuccessful) {
					v("Loggedin Anonymously")
					onCompleted()
				}
			}
		}
	}


	fun registerToken(refreshedToken: String?) {
		if (currentUser == null) {
			signInAnonymously {
				uploadToken(refreshedToken)
				v("Test")
			}
		} else {
			uploadToken(refreshedToken)
		}
	}

	fun uploadToken(refreshedToken: String?) {
		refreshedToken?.let {
			if (!refreshedToken.isEmptyString()) {

				val userData = HashMap<String, String>()
				userData.put("instanceId", refreshedToken)


				updateData(userData)

			}

		}
	}

	fun updateData(userData: HashMap<String, String>) {
		currentUser?.let {
			val userProperties = FirebaseDb.userProperties
			val uid = it.uid
			if (!uid.isEmptyString()) {

				val child = userProperties.child(uid)
				child.updateChildren(userData as Map<String, Any>?)
			}
		}
	}

	fun getUserData(onDataUpdated: (UserInfoModel) -> Unit, onError: (String) -> Unit = {}) {
		currentUser?.let {
			val userProperties = FirebaseDb.userProperties
			val uid = it.uid
			if (!uid.isEmptyString()) {
				userProperties.child(uid).addListenerForSingleValueEvent(object : ValueEventListener {
					override fun onCancelled(error: DatabaseError?) {
						val message = error?.details ?: ""
						onError(message)
					}

					override fun onDataChange(data: DataSnapshot?) {
						v("Getting datasnapshot child $data")
						data?.let {
							if (lifeCycle?.currentState?.isAtLeast(Lifecycle.State.CREATED) ?: false) {
								if (it.hasChild("shortLink")) {
									val shareUrl = it.child("shortLink").value.toString()
									val originalUrl = it.child("previewLink").value.toString()
									val userInfo = UserInfoModel(shareUrl, originalUrl)
									onDataUpdated(userInfo)
								}
							}
						}
					}

				})
			}
		}
	}

}