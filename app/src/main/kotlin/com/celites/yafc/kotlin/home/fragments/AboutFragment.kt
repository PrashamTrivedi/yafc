package com.celites.yafc.kotlin.home.fragments

import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.celites.kutils.browseUrl
import com.celites.kutils.getDefaultSharedPreferences
import com.celites.kutils.setVisible
import com.celites.kutils.startActivity
import com.celites.kutils.versionName
import com.celites.yafc.BuildConfig
import com.celites.yafc.R
import com.celites.yafc.kotlin.Utils.startDebugActivity
import com.celites.yafc.kotlin.faq.FaqActivity
import kotlinx.android.synthetic.main.layout_about.*

/**
 * Created by Prash on 12-03-2017.
 */
class AboutFragment : BottomSheetDialogFragment() {
	override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater?.inflate(R.layout.layout_about, container, false)
	}

	override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		val s = "Version - ${context.versionName}"
		versionText.text = s

		faqItem.setOnClickListener {
			startActivity<FaqActivity>()
		}

		showPrefs.setVisible(BuildConfig.DEBUG)
		showPrefs.setOnClickListener {
			it.context.startDebugActivity()
		}

		betaText.setVisible(context.getDefaultSharedPreferences().getBoolean("BETA_AVAILABLE", true))

		betaText.setOnClickListener {
			it.context.browseUrl("https://plus.google.com/communities/112697972745866938813?sqinv=OTM3SUxGMjRYMXVndUh3MUoxRUpURmxicVA5ZnNn")
		}
	}


}



