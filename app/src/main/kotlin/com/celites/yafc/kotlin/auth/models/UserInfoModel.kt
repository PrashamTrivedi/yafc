package com.celites.yafc.kotlin.auth.models

/**
 * Created by Prash on 12-03-2017.
 */
data class UserInfoModel(var shortLink: String, var longLink: String, var extraJsonData: String = "");