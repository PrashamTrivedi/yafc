package com.celites.yafc.kotlin.tutorial.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.celites.kutils.isEmptyString
import com.celites.yafc.R
import kotlinx.android.synthetic.main.fragment_tutorial.*

/**
 * Created by Prash on 03-03-2017.
 */
class TutorialFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        readBundle()

    }

    var tutorialExplanation = "";
    var tutorialDrawable = -1;

    private fun readBundle() {
        val args = arguments
        if (args.containsKey(getString(R.string.bundle_tutorial_text))) {
            tutorialExplanation = args.getString(getString(R.string.bundle_tutorial_text))
        } else {
            tutorialExplanation = ""
        }
        if (args.containsKey(getString(R.string.bundle_tutorial_drawable))) {
            tutorialDrawable = args.getInt(getString(R.string.bundle_tutorial_drawable))
        } else {
            tutorialDrawable = -1
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_tutorial, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (tutorialDrawable != -1 && !tutorialExplanation.isEmptyString()) {
            tutorialImage.setImageResource(tutorialDrawable)
            tutorialText.setText(tutorialExplanation)
        }
    }
}