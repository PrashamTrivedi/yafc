package com.celites.yafc.kotlin.home.arch

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.support.v4.provider.DocumentFile
import com.celites.kutils.hasPreview
import com.celites.kutils.isAudio
import com.celites.kutils.isImage
import com.celites.kutils.isVideo
import com.celites.yafc.R
import com.celites.yafc.kotlin.Utils.getFiles
import com.celites.yafc.kotlin.home.models.FileModel
import com.celites.yafc.kotlin.home.viewmodels.FileViewModel


/**
 * Created by Prash on 10-07-2017.
 */
class FilesPresenter(application: Application, val path: String) : AndroidViewModel(application) {

	var selectedFileType: String = ""
		set(value) {
			field = value
		}

	val files: MutableLiveData<List<FileViewModel>>
		get() {
			val toList = getFlies()

			val data = MutableLiveData<List<FileViewModel>>()
			data.value = toList
			return data
		}


	fun refreshFiles() {
		files.postValue(getFlies())
	}

	private fun getFlies(): List<FileViewModel> {
		val toList = path.getFiles(getApplication(), true).map {
			val size = it.length()
			val filePath = it.uri.toString()
			val name = it.name
			val previewUri = if (it.isVideo || it.isImage) filePath else ""
			val file = FileModel(filePath, name, previewUri, size)
			file.documentFile = it
			val iconId = prepareIconId(it)
			file.iconId = iconId
			val hasPreview = it.hasPreview
			val fileViewModel = FileViewModel(getApplication(), file, hasPreview)
			var isSelected = false
			if (selectedFileType.equals("ALL", true)) {
				isSelected = false
			} else if (selectedFileType.equals("NONE", true)) {
				isSelected = false

			}
			fileViewModel.isSelected = isSelected
			fileViewModel
		}.toList()
		return toList
	}


	private fun prepareIconId(documentFile: DocumentFile): Int {
		when {
			documentFile.isDirectory -> return R.drawable.ic_folder_black_24dp
			documentFile.isAudio -> return R.drawable.ic_music_note_black_24dp
			else -> return R.drawable.ic_insert_drive_file_black_24dp
		}
	}


	class Factory(val application: Application, val path: String) : ViewModelProvider.NewInstanceFactory() {
		override fun <T : ViewModel> create(modelClass: Class<T>): T {

			return FilesPresenter(application, path) as T
		}
	}
}