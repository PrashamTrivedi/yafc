package com.celites.yafc.kotlin.tutorial.adapters

import android.content.Context
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.celites.yafc.R
import com.celites.yafc.kotlin.data.TutorialData
import com.celites.yafc.kotlin.tutorial.fragments.TutorialFragment

/**
 * Created by Prasham on 2/21/2016.
 */
class TutorialAdapter(var context: Context, var fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {

    var tutorialsList: List<TutorialData> = arrayListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    var bundle: Bundle? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    override fun getItemPosition(`object`: Any?): Int {
        return POSITION_NONE
    }


    fun getTab(position: Int) = tutorialsList.get(position)
    override fun getCount(): Int = tutorialsList.size

    override fun getItem(p0: Int): TutorialFragment {
        val localBundle = Bundle()
        localBundle.putString(context.getString(R.string.bundle_tutorial_text), getTab(p0).text)
        localBundle.putInt(context.getString(R.string.bundle_tutorial_drawable), getTab(p0).drawableRes)

        bundle?.let {
            localBundle.putAll(bundle)
        }

        val fragment = TutorialFragment()
        fragment.arguments = localBundle
        return fragment

    }

    override fun getPageTitle(position: Int): CharSequence? {
        return getTab(position).text
    }

}