package com.celites.yafc.kotlin.home

import android.app.Activity
import android.arch.lifecycle.LifecycleRegistry
import android.arch.lifecycle.LifecycleRegistryOwner
import android.content.Intent
import android.os.Bundle
import android.support.v4.provider.DocumentFile
import android.support.v7.app.AppCompatActivity
import com.celites.kutils.isEmptyString
import com.celites.yafc.R
import com.celites.yafc.kotlin.data.DataRepository
import com.celites.yafc.kotlin.home.adapters.GridAdapter
import com.celites.yafc.kotlin.home.fragments.WatchedFileFragment

/**
 * Created by Prash on 04-03-2017.
 */
class SubdirectoryActivity : AppCompatActivity(), GridAdapter.OnDirectoryClickListener, LifecycleRegistryOwner {
	val lifeCycleRegistry: LifecycleRegistry by lazy {
		LifecycleRegistry(this)
	}

	override fun getLifecycle() = lifeCycleRegistry

    override fun onDirectoryClick(documentFile: DocumentFile?) {
        documentFile?.let {
            val tabModel = dataRepo.getTabByPath(it.uri.toString())
            if (tabModel == null) {
                val path = it.uri.toString()
                val fragment = createFragment(path, it.name)
                supportFragmentManager.beginTransaction().replace(R.id.fragmentContainer, fragment, path).addToBackStack(path).commit()
            } else {
                val bundle = Bundle()
                bundle.putString(getString(R.string.path_to_open), it.uri.toString())
                val intent = Intent()
                intent.putExtras(bundle)
                setResult(Activity.RESULT_OK, intent)
            }
        }
    }

    val dataRepo by lazy {
        DataRepository().initWith(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subdirectory)
        val bundle = savedInstanceState ?: intent.extras
        val path = bundle.getString(getString(R.string.bundle_file_path), "")
        val name = bundle.getString(getString(R.string.bundle_dir_name), "")
        if (!path.isEmptyString()) {
            val fragment = createFragment(path, name)
            supportFragmentManager.beginTransaction().replace(R.id.fragmentContainer, fragment, path).commit()
        }
    }

    private fun createFragment(path: String?, dirName: String): WatchedFileFragment {
        val localBundle = Bundle()
        localBundle.putString(getString(R.string.bundle_file_path), path)
        localBundle.putBoolean(getString(R.string.bundle_is_peeking), true)
        localBundle.putString(getString(R.string.bundle_dir_name), dirName)


        val fragment = WatchedFileFragment()
        fragment.arguments = localBundle
        fragment.onDirectoryClickListener = this@SubdirectoryActivity
        return fragment
    }
}