package com.celites.yafc.kotlin.Utils

import android.support.v7.util.AsyncListUtil
import com.celites.yafc.kotlin.home.models.TabMetadata

/**
 * Created by Prasham on 10/25/2016.
 */
class TabMetaDataUtil(var dataCallBack: DataCallback<TabMetadata>, var viewCallBack: ViewCallback) : AsyncListUtil<TabMetadata>(TabMetadata::class.java, 5, dataCallBack, viewCallBack) {
}