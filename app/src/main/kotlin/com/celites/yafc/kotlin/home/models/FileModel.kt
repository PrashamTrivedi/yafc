package com.celites.yafc.kotlin.home.models

import android.support.v4.provider.DocumentFile
import com.celites.yafc.R
import io.realm.RealmObject
import io.realm.annotations.Ignore
import io.realm.annotations.PrimaryKey

/**
 * Created by Prasham on 3/17/2016.
 */
open class FileModel(@PrimaryKey open var path: String = "", open var name: String = "", open var previewUrl: String = "", open var size: Long = 0) : RealmObject() {

    @Ignore open
    var documentFile: DocumentFile? = null
        set(value) {
            field = value
        }
    @Ignore open var iconId: Int = R.drawable.ic_folder_black_24dp
        set(value) {
            field = value
        }
}