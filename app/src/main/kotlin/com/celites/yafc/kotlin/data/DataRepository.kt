package com.celites.yafc.kotlin.data

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import com.celites.kutils.getDefaultSharedPreferences
import com.celites.kutils.v
import com.celites.yafc.kotlin.home.models.TabModel
import io.realm.Realm
import io.realm.RealmConfiguration
import kotlin.properties.Delegates

/**
 * Created by Prasham on 9/4/2016.
 */
class DataRepository : LifecycleObserver {
	lateinit var context: Context
	private var realm: Realm by Delegates.notNull()
	private var realmConfig: RealmConfiguration by Delegates.notNull()

	var lifeCycle: Lifecycle? = null
		set(value) {
			field = value
			field?.addObserver(this)
		}

	fun initWith(context: Context): DataRepository {
		this.context = context
		Realm.init(context)
		realmConfig = RealmConfiguration.Builder().build()
		realm = Realm.getInstance(realmConfig)
		return this
	}

	fun doNothing(): (TabModel) -> Unit = {}


	fun addTab(tabModelToInsert: TabModel, onFinished: (tabModel: TabModel) -> Unit = {},
	           onError: (tabModel: TabModel, message: String) -> Unit = { _, _ -> }, onMaxTabReached: (tabModel: TabModel) -> Unit = {}) {
		if (canAddMoreTabs()) {
			val currentMaxId = realm.where(TabModel::class.java)?.max("id")?.toInt() ?: 0
			try {
				realm.executeTransaction {
					val tabModel = realm.createObject(TabModel::class.java, currentMaxId + 1)
					tabModel.let {
						it.path = tabModelToInsert.path
						it.name = tabModelToInsert.name
						it.place = tabModelToInsert.place
						it.scanDurationInHours = tabModelToInsert.scanDurationInHours
						it.nukeDurationInHours = tabModelToInsert.nukeDurationInHours
					}

					it.copyToRealmOrUpdate(tabModel)
					onFinished(tabModel)
				}
			} catch(e: Exception) {
				onError(tabModelToInsert, e.toString())
				v("Error in inserting tab")
			}
		} else {
			onMaxTabReached(tabModelToInsert)
		}
	}

	fun canAddMoreTabs() = (getNoOfTabs() < getAllowedTabs())
	//    public fun canAddMoreTabs() = (BuildConfig.DEBUG) || (getNoOfTabs() < getAllowedTabs())


	fun getAllowedTabs() = Math.max(5, context.getDefaultSharedPreferences().getInt("MAX_ALLOWED_TABS", 5))

	fun getNoOfTabs() = getAllTabs().size

	fun getAllTabs() = realm.where(TabModel::class.java).findAll().toMutableList()

	fun getTabByPath(path: String): TabModel? = realm.where(TabModel::class.java).equalTo("path", path).findFirst()

	fun getAllTabsSorted() = realm.where(TabModel::class.java).findAllSorted("place").toMutableList()

	@OnLifecycleEvent(Lifecycle.Event.ON_DESTROY) fun destroy() = realm.close()

	fun deletePath(path: String, afterDeletion: (Int, Boolean) -> Any = { i: Int, b: Boolean -> }) {
		val index = getAllTabsSorted().indexOf(getTabByPath(path))
		realm.executeTransaction {
			it.where(TabModel::class.java).equalTo("path", path).findAll()?.let {
				afterDeletion(index, it.deleteAllFromRealm())
			}
		}
	}


}