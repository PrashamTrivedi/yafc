package com.celites.yafc.kotlin.home.viewmodels

import android.content.Intent
import android.os.Build
import android.support.v4.provider.DocumentFile
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.celites.kutils.getDefaultSharedPreferences
import com.celites.kutils.negativeButton
import com.celites.kutils.platformIsGreaterThan
import com.celites.kutils.positiveButton
import com.celites.kutils.showDialog
import com.celites.yafc.kotlin.InAppPurchaseUtils
import com.celites.yafc.kotlin.Utils.getSnackbar
import com.celites.yafc.kotlin.data.DataRepository
import com.celites.yafc.kotlin.home.adapters.PageAdapter
import com.celites.yafc.kotlin.home.models.TabModel
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crash.FirebaseCrash

/**
 * Created by Prasham on 2/22/2016.
 */
class MainActivityViewModel(val activity: AppCompatActivity) : PageAdapter.FileAddRequestClickListener {
    override fun onFileAddRequestClick(view: View?) {
        view?.let { canAdd(it) }
    }


    var pageAdapter: PageAdapter? = null
        set(value) {
            field = value
            field?.fileAddRequestClickListener = this@MainActivityViewModel
        }

    var dataRepository: DataRepository? = null
        set(value) {
            field = value
        }


    val REQUEST_CODE = 2488


	fun getTabsCount() = pageAdapter?.tabsList?.size ?: 0

	fun getAvailableCount(): Int {
		return dataRepository?.getAllowedTabs() ?: 5
	}

	fun canAdd(view: View) {

		if (canAddMoreTabs()) {
			FirebaseAnalytics.getInstance(activity).logEvent("AddingTabs", null)
			addTab()
		} else {
			FirebaseAnalytics.getInstance(activity).logEvent("AddingTabsMaxEventReached", null)
			showSnackbar(view)

		}
	}

    private fun showSnackbar(view: View) {
        val snackbar = activity.getSnackbar(view, "Only ${getAvailableCount()} tabs for free!!")
        snackbar.setAction("Purchase Tabs", {
            FirebaseAnalytics.getInstance(activity).logEvent("StartedPurchaseFlow", null)
            InAppPurchaseUtils.purchase()
        })
        snackbar.show()
    }

    private fun canAddMoreTabs() = dataRepository?.canAddMoreTabs() ?: true

	fun handleActivityResult(view: View, requestCode: Int, resultCode: Int, data: Intent) {
		if (platformIsGreaterThan(Build.VERSION_CODES.LOLLIPOP)) {
			if (requestCode == REQUEST_CODE) {
				val treeUri = data.data

				activity.grantUriPermission(activity.packageName, treeUri, Intent.FLAG_GRANT_READ_URI_PERMISSION)
				val takeFlags = data.flags and (Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)

				activity.getContentResolver().takePersistableUriPermission(treeUri, takeFlags)
				val fromTreeUri = DocumentFile.fromTreeUri(activity, treeUri)
				val path = fromTreeUri.uri.toString()
				if (!path.isEmpty()) {
					val name = fromTreeUri.name

					val tabModelFromRealm = dataRepository?.getTabByPath(path)

					if (tabModelFromRealm == null) {

						val scanHours = activity.getDefaultSharedPreferences().getLong("MIN_SCAN_HOURS", 12)
						val nukeHours = activity.getDefaultSharedPreferences().getLong("MIN_NUKE_HOURS", 24)

						val tabModel = TabModel(path = path, name = name, scanDurationInHours = scanHours, nukeDurationInHours = nukeHours)
						dataRepository?.addTab(tabModel, onFinished = {
							pageAdapter?.addTab(it)
						}, onError = { tabModel, error ->
							FirebaseCrash.log(error)
						}, onMaxTabReached = {
							FirebaseAnalytics.getInstance(activity).logEvent("AddingTabsMaxEventReached", null)
							showSnackbar(view)
						})
					} else {
						val index = tabsList.indexOf(tabModelFromRealm)

						pageAdapter?.notifyDataSetChanged()
					}

				}
			}


		}
	}


	fun pickPath() {

		activity.showDialog {
			setTitle("Pick Path")
			positiveButton("Pick Files", { pickPathToRead() })
			negativeButton()
		}


	}

    private fun pickPathToRead() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
        intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION or Intent.FLAG_GRANT_PREFIX_URI_PERMISSION)

        activity.startActivityForResult(intent, REQUEST_CODE)
    }

	fun addTab() {
		pickPath()
	}

    var tabsList: MutableList<TabModel> = arrayListOf()
        set(value) {
            field = value
            pageAdapter?.tabsList = value
        }


}