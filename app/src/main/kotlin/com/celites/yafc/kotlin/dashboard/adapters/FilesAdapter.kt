package com.celites.yafc.kotlin.dashboard.adapters

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.util.AsyncListUtil
import android.support.v7.widget.RecyclerView
import android.text.Spannable
import android.text.SpannableString
import android.text.style.TextAppearanceSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.celites.kutils.setVisible
import com.celites.yafc.R
import com.celites.yafc.kotlin.home.MainActivity
import com.celites.yafc.kotlin.home.models.TabMetadata
import kotlinx.android.synthetic.main.files_entry.view.*
import kotlinx.android.synthetic.main.row_file_detail.view.*

/**
 * Created by Prasham on 10/23/2016.
 */
class FilesAdapter(var context: Context) : RecyclerView.Adapter<FilesAdapter.FilesViewHolder>() {
    var filesList: MutableList<TabMetadata> = arrayListOf()
        set(value) {
            field = value
//            dataCallback = object : AsyncListUtil.DataCallback<TabMetadata>() {
//                override fun refreshData() = filesList.size
//
//                override fun fillData(data: Array<TabMetadata>, startPosition: Int, itemCount: Int) {
//                    for (i in 0..itemCount) {
//
//                        val tabMetadata = filesList[startPosition + i]
//                        val path = tabMetadata.tabModel.path
//                        path?.let {
//                            val listFiles = it.getDocumentTree(context).listFiles()
//                            val (files, directories) = listFiles.partition { !it.isDirectory }
//                            tabMetadata.noOfFiles = files.size
//                            tabMetadata.noOfDirectories = directories.size
//                            val totalSize = listFiles.map { it.length() }.sum()
//                            tabMetadata.size = Formatter.formatShortFileSize(context, totalSize)
//
//                        }
//                        data[i] = tabMetadata
//                    }
//                }
//            }
        }

    lateinit var dataCallback: AsyncListUtil.DataCallback<TabMetadata>


    var viewCallback: AsyncListUtil.ViewCallback? = null
        set(value) {
            field = value
        }

    override fun onBindViewHolder(holder: FilesViewHolder?, position: Int) {

        val tabMetaData = filesList[position]
        holder?.setTabMetaData(tabMetaData)

//        viewCallback?.let {
//            if (filesList.isNotEmpty()) {
//                var tabMetaData = TabMetaDataUtil(dataCallback, it).getItem(position)
//                if (tabMetaData == null) {
//                    tabMetaData = TabMetadata(filesList[position].tabModel, size = "Getting Data")
//                }
//                holder?.setTabMetaData(tabMetaData)
//            }
//        }
    }


    var layoutInflater: LayoutInflater

    init {
        layoutInflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int) = FilesViewHolder(layoutInflater.inflate(R.layout.files_entry, parent, false))

    override fun getItemCount() = filesList.size

    class FilesViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun setTabMetaData(tabMetadata: TabMetadata) {
            itemView.directoryName.text = tabMetadata.tabModel.name
            val directories = "${tabMetadata.noOfDirectories} directories"
            val files = "${tabMetadata.noOfFiles} files"
            val size = tabMetadata.size

            val filesNDirectories = "$files\n$directories"
            val spannableString = SpannableString("$filesNDirectories $size")
            val textAppearanceSmallSpan = TextAppearanceSpan(view.context, R.style.smallTextStyle)
            val textAppearanceBigSpan = TextAppearanceSpan(view.context, R.style.smallTextStyle)


            spannableString.setSpan(textAppearanceSmallSpan, 0, filesNDirectories.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            spannableString.setSpan(textAppearanceBigSpan, filesNDirectories.length, spannableString.length, Spannable
                    .SPAN_INCLUSIVE_INCLUSIVE)
            itemView.directoryDescription.text = spannableString
            itemView.imagePreview.setVisible(false)
//            itemView.createdDate.text = tabMetadata.getCreatedDate()

            itemView.setOnClickListener {
                val bundle = Bundle()
                bundle.putString(view.context.getString(R.string.bundle_selected_path), tabMetadata.tabModel.path)
                val intent = Intent(view.context, MainActivity::class.java)
                intent.putExtras(bundle)
                view.context.startActivity(intent)
            }
        }
    }


}