package com.celites.yafc.kotlin.fcm

import android.app.PendingIntent
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.support.v4.app.TaskStackBuilder
import com.celites.kutils.setNotification
import com.celites.yafc.R
import com.celites.yafc.kotlin.home.MainActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

/**
 * Created by Prasham on 9/21/2016.
 */
class MyFCMService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {

        remoteMessage?.let {
            it.notification?.let {
                this@MyFCMService.setNotification(2488, {
                    setContentTitle(it.title)
                    setContentText(it.body)
                    setColor(getColor(R.color.colorPrimary))
                    setSmallIcon(R.drawable.ic_notification_icon)

                    setAutoCancel(true)
                    val bigTextStyle = NotificationCompat.BigTextStyle(this)
                    bigTextStyle.setBigContentTitle(it.title)
                    bigTextStyle.bigText(it.body)
                    bigTextStyle.setSummaryText(it.body)

                    setStyle(bigTextStyle)
                    val intent = Intent(this@MyFCMService, MainActivity::class.java)
                    intent.putExtras(remoteMessage.data.extractBundle())
                    val stackBuilder: TaskStackBuilder = TaskStackBuilder.create(this@MyFCMService)
                    stackBuilder.addNextIntent(intent)
                    val pendingIntent = stackBuilder.getPendingIntent(2488, PendingIntent.FLAG_UPDATE_CURRENT)
                    setContentIntent(pendingIntent)
                })
            }
        }
    }

}

private fun  Map<String, String>.extractBundle(): Bundle {
    val bundle = Bundle()
    val toTypedArray = this.toList().toTypedArray()
    return bundle.put(toTypedArray)
}

private fun Bundle.put(params: Array<Pair<String, String>>): Bundle {
    params.forEach {
        val key = it.first
        val value = it.second
        when (value) {
            is CharSequence -> putCharSequence(key, value)
            is String -> putString(key, value)

        }
    }
    return this
}
