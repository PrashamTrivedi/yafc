package com.celites.yafc.kotlin

import android.app.Activity
import android.content.Context
import com.celites.kutils.edit
import com.celites.kutils.getDefaultSharedPreferences
import org.solovyev.android.checkout.ActivityCheckout
import org.solovyev.android.checkout.Billing
import org.solovyev.android.checkout.Billing.DefaultConfiguration
import org.solovyev.android.checkout.BillingRequests
import org.solovyev.android.checkout.Checkout
import org.solovyev.android.checkout.Inventory
import org.solovyev.android.checkout.ProductTypes.IN_APP
import org.solovyev.android.checkout.Purchase
import org.solovyev.android.checkout.RequestListener
import org.solovyev.android.checkout.ResponseCodes


/**
 * Created by Prash on 06-01-2017.
 */
object InAppPurchaseUtils {
    lateinit var billing: Billing
    lateinit var context: Context
    lateinit var checkout: ActivityCheckout
    lateinit var makeInventory: Inventory

	fun initBilling(context: Context) {

		billing = Billing(context, object : DefaultConfiguration() {
			override fun getPublicKey() = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkWod4/prA6m7dvrkSQXgzqSU49AlrRh6atopC4EPyrbtqHxbFhDL6DM3jPcg4YLLgzefwmF/gutRy5uNtxZdzEihZg+yBP9JwHenuPgNGHW3zqJdCgujMwmc0ECVhGaovB8AHb+7T495HRzbpxVy4ejSSUEh0+Jggbmc4IqJyR4u0bvbq5qBO4DvlJ1KgzdDrzy8Kd4nn6h8HTTYkFHppKo21HPM595q8RV/woGp871iADqauPi+kBMacesycuKV0D1rPdQjJaPCbP2d8hiAQehEmap1ApzK5At3d3R4zodSP7BC4Cns62qBakVvwoMLmFxI1W7mG5UE9gKQgKKAvQIDAQAB"


		})
	}


	fun onPurchased(result: Purchase) {
		var canAdd10Tabs = result.sku.equals("tabs_10", true)
		var tabsToAdd = if (canAdd10Tabs) 10 else 5
		onPurchased(tabsToAdd)
	}

    private fun onPurchased(tabsToAdd: Int) {
        var currentMaxAllowedTabs = context.getDefaultSharedPreferences().getInt("MAX_ALLOWED_TABS", 5)
        context.getDefaultSharedPreferences().edit {
            putInt("MAX_ALLOWED_TABS", currentMaxAllowedTabs + tabsToAdd)
        }
    }

	fun initCheckout(activity: Activity, requestCode: Int = 0, successMethod: (result: Purchase) -> Unit = {},
	                 errorMethod: (response: Int, e: Exception) -> Unit = { i: Int, exception: Exception -> }) {
		context = activity
		initBilling(activity)
		checkout = Checkout.forActivity(activity, billing)
		checkout.let {
			it.start()
			it.createPurchaseFlow(object : RequestListener<Purchase> {
				override fun onSuccess(result: Purchase) {
					onPurchased(result)
					successMethod(result)
				}

				override fun onError(response: Int, e: Exception) {
					if (response == ResponseCodes.ITEM_ALREADY_OWNED) {
						onPurchased(10)
					} else {
						errorMethod(response, e)
					}
				}

			})

			makeInventory = it.makeInventory()
		}
	}


	fun purchase(skuName: String = "tabs_10") {
		checkout.whenReady(object : Checkout.EmptyListener() {
			override fun onReady(requests: BillingRequests) {
				requests.purchase(IN_APP, skuName, "Purchase", checkout.purchaseFlow)
			}
		})
	}


}