package com.celites.yafc.kotlin.tutorial

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import com.celites.kutils.setVisible
import com.celites.yafc.R
import com.celites.yafc.kotlin.data.TutorialData
import com.celites.yafc.kotlin.tutorial.adapters.TutorialAdapter
import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.android.synthetic.main.activity_tutorial.*

/**
 * Created by Prash on 02-03-2017.
 */
class TutorialActivity : AppCompatActivity() {
    val data: List<TutorialData> = listOf(TutorialData("Start Wacthing", R.drawable.t_1), TutorialData("Step 1: Open File Picker", R.drawable.t_2), TutorialData("Step 2: Select your desired directory", R.drawable.t_3), TutorialData("Step 3: Your Files Are being Watched now,\n Select one item", R
            .drawable.t_4), TutorialData("Step 4: You can delete or move them. ", R.drawable.t_4), TutorialData("You can have more then one directories to watch, Just repeat step 1 to step 3.", R.drawable.t_5), TutorialData("If your directory has subdirectories, they " +
            "will be handled as well", R.drawable.t_5), TutorialData("This app will also show notifications to tell you about new files and will " +
            "remind you to delete all files", R.drawable.t_6))
    val firebaseAnalytics: FirebaseAnalytics by lazy { FirebaseAnalytics.getInstance(this) }
    var currentPosition = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tutorial)
        val tutorialAdapter = TutorialAdapter(this, supportFragmentManager)
        tutorialAdapter.tutorialsList = data
        tutorialPager.adapter = tutorialAdapter

        firebaseAnalytics.setCurrentScreen(this, "Tutorial", "Tutorial Screen")

        prevButton.setVisible(false, false)
        tutorialPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                currentPosition = position
                prevButton.setVisible(position != 0, false)
                if (position == data.size - 1) {
                    nextButton.text = getString(R.string.label_done)
                } else {
                    nextButton.text = getString(R.string.label_next)
                }
            }

        })

        prevButton.setOnClickListener {
            tutorialPager.setCurrentItem(currentPosition - 1, true)
        }
        nextButton.setOnClickListener {
            if (currentPosition == data.size - 1) {
                finish()
            } else {
                tutorialPager.setCurrentItem(currentPosition + 1, true)
            }
        }

    }
}