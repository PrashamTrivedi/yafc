package com.celites.yafc.kotlin.Utils

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.ActionMode
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager

/**
 * Created by Prasham on 2/22/2016.
 */

fun Bundle.extractBundleData(keyValSeparator: String = ": ", newSetSeparator: String = "\n"): String {
    val keyvals: StringBuilder = StringBuilder();
    keySet().forEach { keyvals.append(it).append(keyValSeparator).append(get(it)).append(newSetSeparator) }
    return keyvals.toString();
}

fun ImageView.setUrlWithGlide(uri: Uri, requestManager: RequestManager, iconId: Int = 0) =
        requestManager.load(uri).placeholder(iconId).error(iconId).into(this)


fun ImageView.setUrlWithFragment(uri: Uri, fragment: Fragment) {
    val with = Glide.with(fragment)
    with.load(uri).into(this);
}

fun Fragment.setSupportActionbar(toolbar: Toolbar) {
    val appcompatActivity = this.activity as AppCompatActivity?
    appcompatActivity?.setSupportActionBar(toolbar)
}

fun ImageView.setUrlWithActivity(uri: Uri, activity: Activity) {
    Glide.with(activity).load(uri).into(this)
}


fun ActionMode.update(actionModeMethod: ActionMode.() -> Unit) {

    actionModeMethod()
    invalidate()
}

fun Context.showSnackbar(view: View, message: String, duration: Int = Snackbar.LENGTH_LONG) {
    getSnackbar(view, message, duration).show()
}

fun Context.getSnackbar(view: View, message: String, duration: Int = Snackbar.LENGTH_LONG) = Snackbar.make(view, message, duration)

