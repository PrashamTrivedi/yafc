package com.celites.yafc.kotlin.faq.models

/**
 * Created by Prasham on 11/22/2016.
 */
data class Faqs(var id: String, var question: String, var answer: String, var isApproved: Boolean = true)