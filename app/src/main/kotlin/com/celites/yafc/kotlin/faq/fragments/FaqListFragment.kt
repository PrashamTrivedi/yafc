package com.celites.yafc.kotlin.faq.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.celites.kutils.v
import com.celites.yafc.R
import com.celites.yafc.kotlin.Utils.setSupportActionbar
import com.celites.yafc.kotlin.data.FirebaseDb
import com.celites.yafc.kotlin.faq.adapters.FaqsAdapter
import com.celites.yafc.kotlin.faq.models.Faqs
import com.celites.yafc.kotlin.faq.viewmodels.FaqViewModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.layout_faq.*

/**
 * Created by Prasham on 11/24/2016.
 */
class FaqListFragment : Fragment(), FaqsAdapter.OnFaqOpenListener {
    override fun onFaqOpen(faqs: Faqs) {
        val faqDetailFragment = FaqDetailFragment()
        faqDetailFragment.faq = faqs
        fragmentManager.beginTransaction().replace(R.id.faqFragmentContainer, faqDetailFragment, "Detail").addToBackStack(faqs.id).commit()
    }

    var faqList: MutableList<Faqs> = arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.layout_faq, container, false)
    }

    private var faqsAdapter: FaqsAdapter? = null


    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        faqToolbar.title = "Faqs"
        setSupportActionbar(faqToolbar)
        faqs.layoutManager = LinearLayoutManager(context)
        faqs.setHasFixedSize(true)
        faqsAdapter = FaqsAdapter(context)
        faqsAdapter?.setHasStableIds(true)
        faqs.adapter = faqsAdapter
        getData()
    }

    private fun getData() {
        val faqQuestions = FirebaseDb.faqQuestions
        faqQuestions.keepSynced(true)
        faqQuestions.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                faqList.clear()
                for (child in dataSnapshot.children) {
                    v("Getting datasnapshot child $child")
                    val key = child.key.toString()
                    val question = child.child("question").value.toString()
                    val answer = child.child("answer").value.toString()
                    val isApproved = child.child("isApproved").value.toString().toBoolean()
                    val faq = Faqs(key, question, answer, isApproved)
                    faqList.add(faq)
                }
                if (isAdded && context != null) {
                    setFaqAdapter()
                }

            }

        })
    }

    private fun setFaqAdapter() {
        val faqViewModels: MutableList<FaqViewModel> = faqList.filter { it.isApproved }
                .map { FaqViewModel(it, context) }
                .toMutableList()
        faqsAdapter?.faqsList = faqViewModels
        faqsAdapter?.onFaqOpenListener = this
    }
}