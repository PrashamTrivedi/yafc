package com.celites.yafc.kotlin.files.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.celites.kutils.edit
import com.celites.kutils.getDefaultSharedPreferences
import com.celites.yafc.R
import com.google.firebase.analytics.FirebaseAnalytics

/**
 * Created by Prash on 06-01-2017.
 */
class DeleteReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        context?.let {
            val instance = FirebaseAnalytics.getInstance(context)
            val notificationBundle = Bundle()
            val action = intent?.action ?: ""
            notificationBundle.putString("action", action)
            val bundle = intent?.extras
            bundle?.let {
                val path = bundle.getString(context.getString(R.string.bundle_selected_path)) ?: ""
                context.getDefaultSharedPreferences().edit {
                    remove("group_nuke_${path}")
                    remove("new_group_${path}")
                }
                notificationBundle.putString("path", path)
            }
            instance.logEvent("NotificationRemoved", notificationBundle)

        }
    }
}