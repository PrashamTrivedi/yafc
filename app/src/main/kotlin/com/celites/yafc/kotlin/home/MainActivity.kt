package com.celites.yafc.kotlin.home


import android.app.Activity
import android.app.job.JobInfo
import android.arch.lifecycle.LifecycleRegistry
import android.arch.lifecycle.LifecycleRegistryOwner
import android.content.ComponentName
import android.content.Intent
import android.net.UrlQuerySanitizer
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.provider.DocumentFile
import android.support.v7.app.AppCompatActivity
import android.text.format.DateUtils
import android.view.Menu
import android.view.MenuItem
import com.celites.kutils.browseUrl
import com.celites.kutils.d
import com.celites.kutils.edit
import com.celites.kutils.getDefaultSharedPreferences
import com.celites.kutils.isEmptyString
import com.celites.kutils.jobScheduler
import com.celites.kutils.putString
import com.celites.kutils.setVisible
import com.celites.kutils.startActivity
import com.celites.kutils.startActivityForResult
import com.celites.kutils.w
import com.celites.yafc.BuildConfig
import com.celites.yafc.R
import com.celites.yafc.kotlin.InAppPurchaseUtils
import com.celites.yafc.kotlin.Utils.extractBundleData
import com.celites.yafc.kotlin.Utils.showSnackbar
import com.celites.yafc.kotlin.auth.AuthProvider
import com.celites.yafc.kotlin.data.DataRepository
import com.celites.yafc.kotlin.faq.FaqActivity
import com.celites.yafc.kotlin.files.FileNukeService
import com.celites.yafc.kotlin.files.FileWatcherService
import com.celites.yafc.kotlin.home.adapters.GridAdapter
import com.celites.yafc.kotlin.home.adapters.PageAdapter
import com.celites.yafc.kotlin.home.fragments.AboutFragment
import com.celites.yafc.kotlin.home.models.TabModel
import com.celites.yafc.kotlin.home.viewmodels.MainActivityViewModel
import com.celites.yafc.kotlin.remoteconfig.RemoteConfigProvider
import com.celites.yafc.kotlin.tutorial.TutorialActivity
import com.google.android.gms.appinvite.AppInvite
import com.google.android.gms.appinvite.AppInviteInvitation
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.empty_view.*
import java.util.*


class MainActivity : AppCompatActivity(), PageAdapter.OnDeleteFinishedListener, PageAdapter.OnFileSelectionListener, GridAdapter.OnDirectoryClickListener, GoogleApiClient.OnConnectionFailedListener, LifecycleRegistryOwner {

	val lifeCycleRegistry: LifecycleRegistry by lazy {
		LifecycleRegistry(this)
	}

	override fun getLifecycle() = lifeCycleRegistry

	override fun onConnectionFailed(p0: ConnectionResult) {

	}

	override fun onFilesSelected(numberOfSelectedFiles: Int) {
		fab.setVisible(numberOfSelectedFiles == 0)
	}

	override fun onDeleted(index: Int) {
		val allTabsSorted = dataRepo.getAllTabsSorted()
		val areTabsAvailable = allTabsSorted.isNotEmpty()
		updateVisibility(areTabsAvailable)

		pageAdapter.tabsList = allTabsSorted
		pageAdapter.notifyDataSetChanged()
		val tabIndex = if (mainActivityViewModel.getTabsCount() > index) index else 0
		viewPagers.setCurrentItem(tabIndex, true)
	}


	val REQUEST_INVITE = 12112
	val REQUEST_OPEN_SUBDIR = 12345

	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		// Inflate the menu; this adds items to the action bar if it is present.
		menuInflater.inflate(R.menu.menu_main, menu)
		return true
	}

	override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
		val canFaqAccessed = getDefaultSharedPreferences().getBoolean("FAQ_CAN_ASK_QUESTIONS", false)
		menu?.findItem(R.id.action_faq)?.isVisible = canFaqAccessed
		menu?.findItem(R.id.action_help)?.isVisible = showTutorialMenu
		return super.onPrepareOptionsMenu(menu)
	}

	private var showTutorialMenu = true

	val firebaseAnalytics: FirebaseAnalytics by lazy { FirebaseAnalytics.getInstance(this) }


	override fun onDirectoryClick(documentFile: DocumentFile?) {
		documentFile?.let {


			val analyticsBundle = Bundle()
			analyticsBundle.putString("DirectoryName", it.name)


			val tabModel = dataRepo.getTabByPath(it.uri.toString())
			if (tabModel == null) {
				analyticsBundle.putString("Action", "NewActivity")
				val bundleToSend = Bundle()
				bundleToSend.putString(getString(R.string.bundle_file_path), it.uri.toString())
				bundleToSend.putString(getString(R.string.bundle_dir_name), it.name)


				startActivityForResult<SubdirectoryActivity>(bundleToSend, REQUEST_OPEN_SUBDIR)

			} else {
				analyticsBundle.putString("Action", "TabSwitch")
				viewPagers.setCurrentItem(Math.max(dataRepo.getAllTabsSorted().indexOf(tabModel), 0), true)
			}

			firebaseAnalytics.logEvent("DirectoryClick", analyticsBundle)
		}

	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		val id = item.itemId

		if (id == R.id.action_settings) {
			return true
		} else if (id == R.id.action_help) {
			showTutorial()
		} else if (id == R.id.action_share) {
			var shareUrl = getDefaultSharedPreferences().getString("SHARE_URL", "")
			if (shareUrl.isEmptyString()) {
				shareUrl = "https://play.google.com/store/apps/details?id=com.celites.yac"
			}
			inviteUsingOtherMeans(shareUrl)
		} else if (id == R.id.action_faq) {
			openFaqActivity()
		} else if (id == R.id.action_about) {
			showAbout()
		}

		return super.onOptionsItemSelected(item)
	}

	private fun showAbout() {
		val aboutFragment = AboutFragment()
		firebaseAnalytics.logEvent("About", Bundle())
		aboutFragment.show(supportFragmentManager, "about")
	}

	private fun inviteUsingOtherMeans(shareUrl: String?) {
		shareUrl?.let {
			val intent = Intent()
			with(intent) {
				action = Intent.ACTION_SEND
				putExtra(Intent.EXTRA_TEXT, it)
				type = "text/plain"

			}
			startActivity(intent)
		}
	}

	fun openFaqActivity() = startActivity<FaqActivity>()

	private var scanHours: Long = 1

	private var nukeHours: Long = 2


	fun showTutorial() {
		startActivity<TutorialActivity>()
	}

	private val REQUEST_CODE_SAF = 2488
	private var generateNotifications = true


	val dataRepo by lazy {
		DataRepository().initWith(this)
	}


	val mainActivityViewModel by lazy { MainActivityViewModel(this) }
	val pageAdapter by lazy { PageAdapter(this, supportFragmentManager) }


	private var tabsList: MutableList<TabModel> = arrayListOf()
		set(value) {
			field = value
		}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		setSupportActionBar(toolbar)

		scanHours = getDefaultSharedPreferences().getLong("MIN_SCAN_HOURS", 1)
		nukeHours = getDefaultSharedPreferences().getLong("MIN_NUKE_HOURS", 2)

		val bundle = savedInstanceState ?: intent.extras


		val googleApiClient = GoogleApiClient.Builder(this).addApi(AppInvite.API).enableAutoManage(this, this).build()
		FirebaseDynamicLinks.getInstance().getDynamicLink(intent).addOnSuccessListener {
			it?.let {

				val sanitizer = UrlQuerySanitizer()
				sanitizer.allowUnregisteredParamaters = true
				sanitizer.parseUrl(it.link.toString())
				val referal = sanitizer.getValue("refer")
				d(referal)
				val userData = HashMap<String, String>()
				userData.put("referal", referal)
				AuthProvider.updateData(userData)
			}
		}


		AuthProvider.lifeCycle = lifecycle

		mainActivityViewModel.dataRepository = dataRepo

		dataRepo.lifeCycle = lifecycle

		tabsList = dataRepo.getAllTabsSorted()

		prepareAdapter()
		viewPagers.adapter = pageAdapter
		tabs.setupWithViewPager(viewPagers)
		val areTabsAvailable = tabsList.isNotEmpty()
		if (areTabsAvailable) {
			mainActivityViewModel.tabsList = tabsList


			bundle?.let {

				if (it.containsKey(getString(R.string.bundle_notification_url))) {
					val url = bundle.getString(getString(R.string.bundle_notification_url))
					if (!url.isEmptyString()) {
						browseUrl(url)
					}
				}
				w("MainActivity ${bundle.extractBundleData()}")

				generateNotifications = it.getBoolean(getString(R.string.bundle_generate_notifications))
				if (it.containsKey(getString(R.string.bundle_selected_path))) {
					val path = it.getString(getString(R.string.bundle_selected_path))
					val index = getIndexOfPath(path)
					if (index != -1) {
						tabs.getTabAt(index)?.select()
						pageAdapter.bundle = bundle
					}
				}
			}
			if (generateNotifications) {
				setJobs(tabsList)
			}
			val analyticsBundle = Bundle()
			analyticsBundle.putString("TabsSaved", tabsList.size.toString())
			firebaseAnalytics.logEvent("FirstLoad", analyticsBundle)
		} else {
			startWatching.setOnClickListener { mainActivityViewModel.canAdd(it) }
			learnMore.setOnClickListener { showTutorial() }
		}


		updateVisibility(areTabsAvailable)

		fab.setOnClickListener { mainActivityViewModel.canAdd(it) }

		firebaseAnalytics.setUserProperty("IsDebug", if (BuildConfig.DEBUG) "True" else "False")

		authenticateUser()

		configureInApp()
	}

	private fun getIndexOfPath(path: String): Int {
		var index = -1
		path.let {

			val tabModel = dataRepo.getTabByPath(it)
			index = tabsList.indexOf(tabModel)

		}
		return index
	}

	private fun configureInApp() {

		InAppPurchaseUtils.initCheckout(this, successMethod = {
			val currentMaxAllowedTabs = getDefaultSharedPreferences().getInt("MAX_ALLOWED_TABS", 5)
			showSnackbar(fab, "congrats! Now you can use $currentMaxAllowedTabs")
		}, errorMethod = { _: Int, exception: Exception ->
			showSnackbar(fab, exception.message ?: "Error in purchase")
		})
	}

	private fun prepareAdapter() {
		pageAdapter.onDeletedClickListener = this
		pageAdapter.onFileSelectionListener = this
		pageAdapter.onDirectoryClickListener = this
		mainActivityViewModel.pageAdapter = pageAdapter
	}


	private fun authenticateUser() {
		AuthProvider.signInAnonymously {

			getDefaultSharedPreferences().edit {
				AuthProvider.currentUser?.uid
			}

			firebaseAnalytics.setUserId(AuthProvider.currentUser?.uid)
			AuthProvider.uploadToken(FirebaseInstanceId.getInstance().token)
		}
	}

	private fun setJobs(tabsList: MutableList<TabModel>) {

		jobScheduler?.let {
			it.cancelAll()

			for (tab in tabsList) {
				val serviceComponent = ComponentName(this@MainActivity, FileWatcherService::class.java)

				val scanIdCode = "Scan:${tab.id}".hashCode()
				val builder = JobInfo.Builder(scanIdCode, serviceComponent)
				builder.apply {
					setPersisted(true)
					setPeriodic((Math.max(scanHours, tab.scanDurationInHours) * DateUtils.HOUR_IN_MILLIS))
					val bundle = PersistableBundle()

					bundle.putString(getString(R.string.bundle_file_path), tab.path)
					bundle.putString(getString(R.string.bundle_dir_name), tab.name)
					bundle.putInt(getString(R.string.bundle_id), tab.id)
					setExtras(bundle)
				}
				val build = builder.build()
				val schedule = it.schedule(build)


				val nukeServiceComponent = ComponentName(this@MainActivity, FileNukeService::class.java)

				val nukeIdCode = "Nuke:${tab.id}".hashCode()
				val nukeBuilder = JobInfo.Builder(nukeIdCode, nukeServiceComponent)
				nukeBuilder.apply {
					setPersisted(true)
					setPeriodic((Math.max(nukeHours, tab.nukeDurationInHours) * DateUtils.HOUR_IN_MILLIS))
					val bundle = PersistableBundle()

					bundle.putString(getString(R.string.bundle_file_path), tab.path)
					bundle.putString(getString(R.string.bundle_dir_name), tab.name)
					bundle.putInt(getString(R.string.bundle_id), tab.id)
					setExtras(bundle)
				}
				val nukeBuild = nukeBuilder.build()
				val nukeSchedule = it.schedule(nukeBuild)
			}

		}


	}

	override fun onResume() {
		super.onResume()
		RemoteConfigProvider.updateRemoteConfig(this, afterSaved = {
			invalidateOptionsMenu()
		})
		AuthProvider.getUserData(onDataUpdated = {
			getDefaultSharedPreferences().putString("SHARE_URL", it.shortLink)
			getDefaultSharedPreferences().putString("ORIGINAL_URL", it.longLink)
		}, onError = {

		})
	}


	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		super.onActivityResult(requestCode, resultCode, data)
		if (requestCode == REQUEST_OPEN_SUBDIR) {
			if (resultCode == RESULT_OK) {
				val path = data?.extras?.getString(getString(R.string.path_to_open), "") ?: ""
				val index = getIndexOfPath(path)
				if (index != -1) {
					tabs.getTabAt(index)?.select()
				}
			}
		}
		if (requestCode == REQUEST_INVITE) {
			if (resultCode == RESULT_OK) {
				data?.let {
					val ids = AppInviteInvitation.getInvitationIds(resultCode, data)
					for (id in ids) {
						d("onActivityResult: sent invitation " + id)
						firebaseAnalytics.logEvent("InvitationSent", null)
						// Get the invitation IDs of all sent messages
					}

				}

			} else {
				// Sending failed or it was canceled, show failure message to the user
				// ...
				firebaseAnalytics.logEvent("InvitationSendFailed", null)
			}
		} else {

			val tabsList = dataRepo.getAllTabsSorted()
			val tabsAvailableAlready = tabsList.isNotEmpty()

			val isResultOk = resultCode != Activity.RESULT_CANCELED

			val bundle = Bundle()
			bundle.putBoolean("WatchResult", isResultOk)
			bundle.putBoolean("TabsAvailable", tabsAvailableAlready)
			firebaseAnalytics.logEvent("WatchResult", bundle)

			val areTabsAvailable = isResultOk || tabsAvailableAlready
			updateVisibility(areTabsAvailable)

			if (isResultOk) {
				mainActivityViewModel.handleActivityResult(fab, requestCode, resultCode, data ?: Intent())
			}
		}

	}


	private fun updateVisibility(areTabsAvailable: Boolean) {
		fab.setVisible(areTabsAvailable)
		tabs.setVisible(areTabsAvailable)
		firstMessageParent.setVisible(!areTabsAvailable)
		showTutorialMenu = areTabsAvailable
		invalidateOptionsMenu()
	}


}
