package com.celites.yafc.kotlin.data

/**
 * Created by Prash on 02-03-2017.
 */
data class TutorialData(val text: String, val drawableRes: Int)