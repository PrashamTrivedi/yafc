package com.celites.yafc.kotlin.home.fragments

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.provider.DocumentFile
import android.support.v7.widget.GridLayoutManager
import android.text.Spannable
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.TextAppearanceSpan
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.celites.kutils.allowOptionsMenu
import com.celites.kutils.containsInArray
import com.celites.kutils.e
import com.celites.kutils.edit
import com.celites.kutils.getDefaultSharedPreferences
import com.celites.kutils.hasPreview
import com.celites.kutils.i
import com.celites.kutils.isAudio
import com.celites.kutils.isEmptyString
import com.celites.kutils.isImage
import com.celites.kutils.isVideo
import com.celites.kutils.positiveButton
import com.celites.kutils.setVisible
import com.celites.kutils.showDialog
import com.celites.yafc.R
import com.celites.yafc.kotlin.InAppPurchaseUtils
import com.celites.yafc.kotlin.Utils.DividerItemDecoration
import com.celites.yafc.kotlin.Utils.KotlinStorageAccessFileWriter
import com.celites.yafc.kotlin.Utils.getFilesFromPath
import com.celites.yafc.kotlin.Utils.showSnackbar
import com.celites.yafc.kotlin.data.DataRepository
import com.celites.yafc.kotlin.home.adapters.GridAdapter
import com.celites.yafc.kotlin.home.adapters.PageAdapter
import com.celites.yafc.kotlin.home.models.FileModel
import com.celites.yafc.kotlin.home.models.TabModel
import com.celites.yafc.kotlin.home.viewmodels.FileViewModel
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crash.FirebaseCrash
import com.trello.rxlifecycle2.components.support.RxDialogFragment
import io.reactivex.Flowable
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter
import kotlinx.android.synthetic.main.layout_fragment.*
import java.util.concurrent.TimeUnit

/**
 * Created by Prasham on 3/3/2016.
 */
class WatchedFileFragment : RxDialogFragment(), GridAdapter.AfterLongClickListener {


	var onDirectoryClickListener: GridAdapter.OnDirectoryClickListener? = null
		set(value) {
			field = value
		}

	var selectedCount = 0
	var count = 0
	override fun afterLongClickListener() {
		count = gridAdapter?.selectedCounts ?: fileViewModels.count { it.isSelected } ?: selectedCount


		val animTime = resources.getInteger(android.R.integer.config_shortAnimTime)
		if (count == 0) {
			hideSelectionToolbar(animTime)

		} else {
			if (actionModeFinished) {

				actionModeFinished = false
			}
			val toolbarTitle = "$count Selected"
			showSelectionToolbar(animTime)
			selectionToolbar.title = toolbarTitle
		}
		onFileSelectionListener?.onFilesSelected(count)

	}

	private fun hideSelectionToolbar(animTime: Int) {
		selectionToolbar.animate().translationY(selectionToolbar.minimumHeight.toFloat()).setDuration(animTime.toLong()).withEndAction {
			selectionToolbar?.setVisible(false)
		}
	}

	private fun showSelectionToolbar(animTime: Int) {
		selectionToolbar.animate().translationY(0.toFloat()).setDuration(animTime.toLong()).withEndAction {
			selectionToolbar?.setVisible(true)
		}
	}

	var fileAddRequestClickListener: PageAdapter.FileAddRequestClickListener? = null
		set(value) {
			field = value
		}

	var onDeleteFinishedListener: PageAdapter.OnDeleteFinishedListener? = null
		set(value) {
			field = value
		}

	var onFileSelectionListener: PageAdapter.OnFileSelectionListener? = null
		set(value) {
			field = value
		}

	private fun prepareForTabDeletion() {
		val snackbar = Snackbar.make(fileList, "Deleting Files", Snackbar.LENGTH_LONG)
		val observable = Flowable.timer(5, TimeUnit.SECONDS).compose { it.observeOn(AndroidSchedulers.mainThread()) }
		val subscription = observable.subscribe({ }, { }, {
			deleteSelected()
		})

		snackbar.setAction("Undo", {
			subscription.dispose()
		})

		snackbar.show()
	}

	private fun deleteSelected() {
		val bundle = Bundle()
		bundle.putString("Action", "Delete")
		bundle.putInt("Count", selectedCount)
		firebaseAnalytics.logEvent("FileAction", bundle)
		gridAdapter?.deleteSelected({
			                            action = ""
			                            selectedCount = 0
			                            refresh()

		                            }, { context.showSnackbar(fileList, "Error in deleting files") })
	}

	val firebaseAnalytics: FirebaseAnalytics by lazy { FirebaseAnalytics.getInstance(context) }


	private fun moveSelected() {

		val bundle = Bundle()
		bundle.putString("Action", "Move")
		bundle.putInt("Count", selectedCount)
		firebaseAnalytics.logEvent("FileAction", bundle)
		safWriter.fragment = this@WatchedFileFragment
		if (safWriter.isExternalDirAvailable()) {

			createDirAndMoveFiles()

		}
	}

	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		safWriter.handleResult(requestCode, resultCode, data, {
			createDirAndMoveFiles()
		})
	}

	private fun createDirAndMoveFiles() {
		val subdirFile = safWriter.createSubdirectory(name)
		subdirFile?.let {
			moveFilesTo(it)
		}
	}

	private fun moveFilesTo(documentFile: DocumentFile) {
		gridAdapter?.moveFilesInList({
			                             action = ""
			                             refresh()
		                             }, { context.showSnackbar(fileList, "Error in deleting files") }, documentFile, safWriter)
	}


	val safWriter by lazy { KotlinStorageAccessFileWriter(REQUEST_CODE) }

	val TAG = "WatchedFileFragment"
	val REQUEST_CODE = 2898
	private var actionModeFinished = false

	val dataRepo: DataRepository by lazy { DataRepository().initWith(context) }

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		allowOptionsMenu()
		readBundle()

	}


	override fun onPrepareOptionsMenu(menu: Menu?) {
		menu?.let {
			it.findItem(R.id.action_refresh).isVisible = !isPeeking
			it.findItem(R.id.action_delete).isVisible = !isPeeking
		}
		super.onPrepareOptionsMenu(menu)
	}

	override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
		super.onCreateOptionsMenu(menu, inflater)
		inflater?.inflate(R.menu.menu_fragment, menu)
	}

	private var gridAdapter: GridAdapter? = null


	override fun onOptionsItemSelected(item: MenuItem?): Boolean {
		when (item?.itemId) {
			R.id.action_refresh -> refresh()
			R.id.action_delete -> prepareForFileDeletion()

		}
		return super.onOptionsItemSelected(item)
	}

	private fun addDirectoryToWatch() {
		val analyticsBundle = Bundle()
		analyticsBundle.putString("Name", name)
		firebaseAnalytics.logEvent("DirectoryAdding", analyticsBundle)
		val tabModelFromRealm = dataRepo.getTabByPath(path)

		if (tabModelFromRealm == null) {

			val scanHours = activity.getDefaultSharedPreferences().getLong("MIN_SCAN_HOURS", 12)
			val nukeHours = activity.getDefaultSharedPreferences().getLong("MIN_NUKE_HOURS", 24)
			val tabModel = TabModel(path = path, name = name, scanDurationInHours = scanHours, nukeDurationInHours = nukeHours)
			dataRepo.addTab(tabModel, onFinished = {

				activity.showSnackbar(view ?: directoryToolbar, "New Tab Added")
			}, onError = { _, error ->
				FirebaseCrash.log(error)
			}, onMaxTabReached = {
				FirebaseAnalytics.getInstance(activity).logEvent("AddingTabsMaxEventReached", null)

				context.showDialog {
					setTitle("Purchase Tabs")
					setMessage("Only ${dataRepo.getAllowedTabs()} tabs for free!!, Click Purchase Tabs to add more tabs")
					positiveButton(text = "Purchase Tabs", handleClick = {
						InAppPurchaseUtils.purchase()
						FirebaseAnalytics.getInstance(activity).logEvent("StartedPurchaseFlow", null)
					})
				}
			})
		}
	}

	fun prepareForFileDeletion() {
		val bundle = Bundle()
		val snackbar = Snackbar.make(fileList, "Deleting Tab", Snackbar.LENGTH_LONG)
		val observable = Flowable.timer(5, TimeUnit.SECONDS).compose { it.observeOn(AndroidSchedulers.mainThread()) }
		val subscription = observable.subscribe({ }, { }, {
			deleteTab()
			bundle.putString("Deletion", "Done")
			firebaseAnalytics.logEvent("FolderDeletion", bundle)
		})

		snackbar.setAction("Undo", {
			bundle.putString("Deletion", "Canceled")
			firebaseAnalytics.logEvent("FolderDeletion", bundle)
			subscription.dispose()
		})

		snackbar.show()

	}

	fun deleteTab() = dataRepo.deletePath(path, { index, isDeleted ->
		if (isDeleted) {
			onDeleteFinishedListener?.onDeleted(index)
		}
	})

	val fileViewModels: MutableList<FileViewModel> = arrayListOf()

	val storageAccessWriter by lazy { KotlinStorageAccessFileWriter(REQUEST_CODE) }

	val glide: RequestManager by lazy { Glide.with(this) }

	lateinit var path: String
	lateinit var name: String
	lateinit var action: String
	var selectedFiles: Array<String> = arrayOf()
	var selectedFileType = ""
	var isPeeking = false

	private fun readBundle() {
		val args = arguments
		args.let {
			//            d("Fragment ${it.extractBundle()}")


			if (args.containsKey(getString(R.string.bundle_file_path))) {
				path = args.getString(getString(R.string.bundle_file_path))
			} else {
				path = ""
			}
			if (args.containsKey(getString(R.string.bundle_dir_name))) {
				name = args.getString(getString(R.string.bundle_dir_name))
			} else {
				name = ""
			}
			if (args.containsKey(getString(R.string.bundle_action))) {
				action = args.getString(getString(R.string.bundle_action))
			} else {
				action = getString(R.string.bundle_action_view)
			}
			if (selectedFileType.isEmptyString()) {
				selectedFileType = args.getString(getString(R.string.bundle_selection_type), getString(R.string.bundle_selection_no_files))
			}
			if (args.containsKey(getString(R.string.bundle_is_peeking))) {
				isPeeking = args.getBoolean(getString(R.string.bundle_is_peeking))
			}
			if (args.containsKey(getString(R.string.bundle_selected_path))) {

				val selectedPath = args.getString(getString(R.string.bundle_selected_path))
				if (selectedPath == path) {
					selectedFileType = args.getString(getString(R.string.bundle_selection_type), getString(R.string.bundle_selection_selected_files))
					if (args.containsKey(getString(R.string.bundle_selected_files))) {
						selectedFiles = args.getStringArray(getString(R.string.bundle_selected_files))
						selectedFiles.let {
							for (string in selectedFiles) {
								i("Checking string $string")
							}
						}
					}

				}

			}
		}
	}

	override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater?.inflate(R.layout.layout_fragment, container, false)
	}

	override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		prepareDirectoryToolbar()

		hideSelectionToolbar(0)

		prepareSelectionToolbar()

		prepareAdapter()

		refresh()


	}

	private fun prepareAdapter() {
		prepareRecyclerView()

		gridAdapter = GridAdapter(context, glide)
		gridAdapter?.setHasStableIds(true)


		(gridAdapter as GridAdapter).afterLongClickListener = this@WatchedFileFragment
		(gridAdapter as GridAdapter).onDirectoryClickListener = onDirectoryClickListener
		fileList.adapter = ScaleInAnimationAdapter(gridAdapter)
	}

	private fun prepareRecyclerView() {
		fileList.layoutManager = GridLayoutManager(context, 2)
		fileList.setHasFixedSize(true)

		fileList.addItemDecoration(DividerItemDecoration(3))
	}

	private fun prepareDirectoryToolbar() {
		directoryToolbar.setVisible(isPeeking)
		if (isPeeking) {
			directoryToolbar.title = name
			directoryToolbar.inflateMenu(R.menu.menu_directory)
			directoryToolbar.setOnMenuItemClickListener {
				when (it?.itemId) {

					R.id.action_add_to_watch -> addDirectoryToWatch()
				}
				true
			}
		}
	}

	private fun prepareSelectionToolbar() {
		selectionToolbar.inflateMenu(R.menu.menu_actionmode)
		selectionToolbar.setOnMenuItemClickListener {
			when (it?.itemId) {
				R.id.action_delete -> prepareForTabDeletion()
				R.id.action_move -> moveSelected()
			}
			true
		}
	}


	fun refresh() {
		progress.setVisible(true)
		errorMessage.setVisible(false)
		fileList.setVisible(false)

		val startTime = System.currentTimeMillis()
		fileViewModels.clear()
		path.getFilesFromPath(context, true).map {
			val size = it.length()
			val path = it.uri.toString()
			val name = it.name
			val previewUri = if (it.isVideo || it.isImage) path else ""
			val file = FileModel(path, name, previewUri, size)
			file.documentFile = it
			val iconId = prepareIconId(it)
			file.iconId = iconId
			val hasPreview = it.hasPreview
			val fileViewModel = FileViewModel(context, file, hasPreview)
			var isSelected = false
			if (selectedFileType.equals(getString(R.string.bundle_selection_all_files), true)) {
				isSelected = false
			} else if (selectedFileType.equals(getString(R.string.bundle_selection_no_files), true)) {
				isSelected = false
			} else {
				if (selectedFiles.isNotEmpty()) {
					isSelected = name.containsInArray(*selectedFiles)
				}
			}
			fileViewModel.isSelected = isSelected
			fileViewModel
		}.toSortedList { fileViewModelLhs, fileViewModelRhs ->
			val lastModifiedLhs = fileViewModelLhs.fileModel.documentFile?.lastModified() ?: 0L
			val lastModifiedRhs = fileViewModelRhs.fileModel.documentFile?.lastModified() ?: 0L
			if (fileViewModelLhs.isDirectory && fileViewModelRhs.isDirectory) lastModifiedLhs.compareTo(
					lastModifiedRhs) else if (fileViewModelLhs.isDirectory) -1 else 1
		}.map {
			if (it.isNotEmpty()) {
				context.getDefaultSharedPreferences().edit {
					putLong("COMPARE_TIME_IN_$name", it.last().fileModel.documentFile?.lastModified() ?: 0L)
				}
			}
			it
		}.compose { it.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()) }.subscribe(
				object : SingleObserver<List<FileViewModel>> {
					override fun onSubscribe(d: Disposable) {

					}

					override fun onError(throwable: Throwable) {
						e(TAG, "Error", throwable as Exception?)
						FirebaseCrash.report(throwable)
						val message = throwable.message ?: "Something gone wrong while getting files"
						showErrorMessage(message)
					}

					override fun onSuccess(t: List<FileViewModel>) {
						fileViewModels.addAll(t)
						progress.setVisible(false)

						if (!fileViewModels.isEmpty()) {
							fileList.setVisible(true)
							gridAdapter?.filesList = fileViewModels
							if (action.equals(getString(R.string.bundle_action_delete), true)) {
								deleteSelected()
							} else if (action.equals(getString(R.string.bundle_action_move), true)) {
								moveSelected()
							}


							val endTime = System.currentTimeMillis()

							val bundle = Bundle()

							bundle.putString("NoOfFiles", fileViewModels.size.toString())
							bundle.putString("LoadingTime", (endTime - startTime).toString())
							firebaseAnalytics.logEvent("TabFiles", bundle)
							afterLongClickListener()

						} else {
							afterLongClickListener()
							val mainMessage = "Congratulations! You have cleared all files from $name.\n"
							val anotherMessage = "If you think something is wrong."
							val secondaryMessage = "$anotherMessage\nclick here."
							val spannable = SpannableString("$mainMessage $secondaryMessage")
							val textAppearanceSmallSpan = TextAppearanceSpan(context, R.style.smallTextStyle)
							val clickSpan = object : ClickableSpan() {
								override fun onClick(widget: View?) {
									fileAddRequestClickListener?.onFileAddRequestClick(widget)
								}

							}
							spannable.setSpan(textAppearanceSmallSpan, mainMessage.length, spannable.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
							spannable.setSpan(clickSpan, mainMessage.length + anotherMessage.length + 1, spannable.length,
							                  Spannable.SPAN_INCLUSIVE_INCLUSIVE)
							showErrorMessage(spannable)
							errorMessage.movementMethod = LinkMovementMethod.getInstance()
						}
					}


				})
	}

	private fun prepareIconId(documentFile: DocumentFile): Int {
		when {
			documentFile.isDirectory -> return R.drawable.ic_folder_black_24dp
			documentFile.isAudio -> return R.drawable.ic_music_note_black_24dp
			else -> return R.drawable.ic_insert_drive_file_black_24dp
		}
	}


	private fun showErrorMessage(message: CharSequence) {
		fileList.setVisible(false)
		progress.setVisible(false)
		errorMessage.setVisible(true)
		errorMessage.text = message
	}

	fun rescan() {
		val intent = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
		intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION or Intent.FLAG_GRANT_PREFIX_URI_PERMISSION)

		activity.startActivityForResult(intent, REQUEST_CODE)
	}

}