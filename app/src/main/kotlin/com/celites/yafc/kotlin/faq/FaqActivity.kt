package com.celites.yafc.kotlin.faq

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.celites.kutils.getDefaultSharedPreferences
import com.celites.kutils.setVisible
import com.celites.yafc.R
import com.celites.yafc.kotlin.faq.fragments.AskQuestionFragment
import com.celites.yafc.kotlin.faq.fragments.FaqListFragment
import com.celites.yafc.kotlin.remoteconfig.RemoteConfigProvider
import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.android.synthetic.main.layout_activity_faq.*

/**
 * Created by Prasham on 11/21/2016.
 */
class FaqActivity : AppCompatActivity() {


    val firebaseAnalytics: FirebaseAnalytics by lazy { FirebaseAnalytics.getInstance(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_activity_faq)
        askQuestion.setVisible(false)
        inflateListFragment()
        askQuestion.setOnClickListener {
            showAskQuestionFragment()
        }

        firebaseAnalytics.setCurrentScreen(this, "Faq", "FAQ Screen")
    }

    private fun showAskQuestionFragment() {
        val analyticsBundle = Bundle()
        analyticsBundle.putString("Asking_Question", "")
        firebaseAnalytics.logEvent("Asking_Question", analyticsBundle)
        AskQuestionFragment().show(supportFragmentManager, "AskQuestion")
    }

    override fun onResume() {
        super.onResume()
        RemoteConfigProvider.updateRemoteConfig(this, {
            askQuestion.setVisible(getDefaultSharedPreferences().getBoolean("FAQ_CAN_ASK_QUESTIONS", false))
        })
    }

    private fun inflateListFragment() {
        supportFragmentManager.beginTransaction().replace(R.id.faqFragmentContainer, FaqListFragment(), "FaqsList").commit()
    }


}