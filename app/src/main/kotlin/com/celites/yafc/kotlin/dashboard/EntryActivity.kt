package com.celites.yafc.kotlin.dashboard

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.util.AsyncListUtil
import android.support.v7.widget.LinearLayoutManager
import android.text.format.DateUtils
import android.text.format.Formatter
import com.celites.kutils.d
import com.celites.kutils.edit
import com.celites.kutils.getDefaultSharedPreferences
import com.celites.kutils.setVisible
import com.celites.yafc.BuildConfig
import com.celites.yafc.R
import com.celites.yafc.kotlin.Utils.DividerItemDecoration
import com.celites.yafc.kotlin.Utils.getDocumentTree
import com.celites.yafc.kotlin.dashboard.adapters.FilesAdapter
import com.celites.yafc.kotlin.data.DataRepository
import com.celites.yafc.kotlin.files.DebugWatcherService
import com.celites.yafc.kotlin.files.FileNukeService
import com.celites.yafc.kotlin.files.FileWatcherService
import com.celites.yafc.kotlin.home.models.TabMetadata
import com.celites.yafc.kotlin.home.models.TabModel
import com.celites.yafc.kotlin.home.viewmodels.MainActivityViewModel
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crash.FirebaseCrash
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.toFlowable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_entry.*

class EntryActivity : AppCompatActivity() {

    val dataRepo by lazy {
        DataRepository().initWith(this)
    }


    val jobSchdeuler by lazy { getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler }
    val filesAdapter: FilesAdapter by lazy {
        FilesAdapter(this)
    }

    val metaDataList: MutableList<TabMetadata> = arrayListOf()

    private var scanHours: Long = 1

    private var nukeHours: Long = 2

    val firebaseAnalytics: FirebaseAnalytics by lazy { FirebaseAnalytics.getInstance(this) }

    val firebaseConfig: FirebaseRemoteConfig by lazy { FirebaseRemoteConfig.getInstance() }

    val mainActivityViewModel by lazy { MainActivityViewModel(this) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_entry)
        setSupportActionBar(toolbar)

        progress.setVisible(true)
        errorMessage.setVisible(false)

        val linearLayoutManager = LinearLayoutManager(this)
        detailRecyclerView.layoutManager = linearLayoutManager
        detailRecyclerView.setHasFixedSize(true)

        detailRecyclerView.addItemDecoration(DividerItemDecoration(3))

        filesAdapter.setHasStableIds(true)
        scanHours = getDefaultSharedPreferences().getLong("MIN_SCAN_HOURS", 1)
        nukeHours = getDefaultSharedPreferences().getLong("MIN_NUKE_HOURS", 2)
        val viewCallback = object : AsyncListUtil.ViewCallback() {
            override fun getItemRangeInto(outRange: IntArray) {
                outRange[0] = linearLayoutManager.findFirstVisibleItemPosition()
                outRange[1] = linearLayoutManager.findLastVisibleItemPosition()
            }

            override fun onItemLoaded(position: Int) {
                filesAdapter.notifyItemChanged(position)
            }

            override fun onDataRefresh() {
                filesAdapter.notifyDataSetChanged()
            }

        }
        filesAdapter.viewCallback = viewCallback
//        gridAdapter = GridAdapter(context, glide)
//        gridAdapter?.setHasStableIds(true)

        getFiles()
    }

    private fun getFiles() {
        val tabsList = dataRepo.getAllTabsSorted()
        val tabsListObservable = tabsList.toFlowable().share()
        prepareAdapter(tabsListObservable)
        setJobs(tabsListObservable)

    }

    private fun setJobs(tabsListObservable: Flowable<TabModel>?) {
        tabsListObservable?.subscribe({
            jobSchdeuler.cancelAll()
            val path = it.path ?: ""
            if (BuildConfig.DEBUG && path.contains("Debug", true)) {
                val serviceComponentTest = ComponentName(this@EntryActivity, DebugWatcherService::class.java)
                val testCode = "Test:${it.id}".hashCode()
                jobSchdeuler.cancel(testCode)
                val builderTest = JobInfo.Builder(testCode, serviceComponentTest)
                builderTest.apply {
                    setPersisted(true)
                    setPeriodic((30 * DateUtils.MINUTE_IN_MILLIS))
                    val bundle = PersistableBundle()

                    bundle.putString(getString(R.string.bundle_file_path), it.path)
                    bundle.putString(getString(R.string.bundle_dir_name), it.name)
                    bundle.putInt(getString(R.string.bundle_id), it.id)
                    setExtras(bundle)
                }
                val buildTest = builderTest.build()
                val scheduleTest = jobSchdeuler.schedule(buildTest)
            }
            val serviceComponent = ComponentName(this@EntryActivity, FileWatcherService::class.java)

            val scanIdCode = "Scan:${it.id}".hashCode()
            val builder = JobInfo.Builder(scanIdCode, serviceComponent)
            builder.apply {
                setPersisted(true)
                setPeriodic((Math.max(scanHours, it.scanDurationInHours) * DateUtils.HOUR_IN_MILLIS))
                val bundle = PersistableBundle()

                bundle.putString(getString(R.string.bundle_file_path), it.path)
                bundle.putString(getString(R.string.bundle_dir_name), it.name)
                bundle.putInt(getString(R.string.bundle_id), it.id)
                setExtras(bundle)
            }
            val build = builder.build()
            val schedule = jobSchdeuler.schedule(build)


            val nukeServiceComponent = ComponentName(this@EntryActivity, FileNukeService::class.java)

            val nukeIdCode = "Nuke:${it.id}".hashCode()
            val nukeBuilder = JobInfo.Builder(nukeIdCode, nukeServiceComponent)
            nukeBuilder.apply {
                setPersisted(true)
                setPeriodic((Math.max(nukeHours, it.nukeDurationInHours) * DateUtils.HOUR_IN_MILLIS))
                val bundle = PersistableBundle()

                bundle.putString(getString(R.string.bundle_file_path), it.path)
                bundle.putString(getString(R.string.bundle_dir_name), it.name)
                bundle.putInt(getString(R.string.bundle_id), it.id)
                setExtras(bundle)
            }
            val nukeBuild = nukeBuilder.build()
            val nukeSchedule = jobSchdeuler.schedule(nukeBuild)
        }, { e ->
            FirebaseCrash.report(e)
        })
    }

    private fun prepareAdapter(tabsListObservable: Flowable<TabModel>) {
        tabsListObservable.map {
            //                        val tabMetadata = filesList[startPosition + i]
            val path = it.path
            val tabModel = it
            var noOfFiles = 0
            var noOfDirectories = 0
            var totalFileSize = "Unknown"
            path?.let {
                val listFiles = it.getDocumentTree(this@EntryActivity).listFiles()
                val (files, directories) = listFiles.partition { !it.isDirectory }
                noOfFiles = files.size
                noOfDirectories = directories.size
                val totalSize = listFiles.map { it.length() }.sum()
                totalFileSize = Formatter.formatShortFileSize(this@EntryActivity, totalSize)

            }
            TabMetadata(tabModel, noOfFiles, noOfDirectories, totalFileSize)
        }.toList().compose {
            it.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()
            )
        }.subscribe()
//                .subscribe(object : Single<List<TabMetadata>>() {
//                    override fun onSubscribe(s: Subscription?) {
//
//                    }
//
//                    override fun onComplete() {
//                        progress.setVisible(false)
//                        if (!metaDataList.isEmpty()) {
//
//                            filesAdapter.filesList = metaDataList
//                            detailRecyclerView.adapter = filesAdapter
//
//                        } else {
//                            showErrorMessage("No Files Available, Add one")
//                        }
//                    }
//
//                    override fun onError(e: Throwable?) {
//                        e("EntryActivity", "Error", e as Exception?)
//                        FirebaseCrash.report(e)
//                        val message = e?.message ?: "Something gone wrong while getting files"
//                        showErrorMessage(message)
//                    }
//
//                    override fun onNext(t: List<TabMetadata>?) {
//                        t?.let {
//                            metaDataList.addAll(it)
//                        }
//                    }
//
//                })
    }

    fun showErrorMessage(message: String) {
        detailRecyclerView.setVisible(false)
        progress.setVisible(false)
        errorMessage.setVisible(true)
        errorMessage.text = message
    }

    override fun onResume() {
        super.onResume()
        updateRemoteConfig()
    }

    private fun updateRemoteConfig() {
        firebaseConfig.setDefaults(R.xml.default_params)

        firebaseConfig.fetch().addOnCompleteListener {
            if (it.isSuccessful) {
                firebaseConfig.activateFetched()
                updateValues()
            } else {
                d("Remote Fetching failed reverting to default")
            }
        }
    }

    private fun updateValues() {
        getDefaultSharedPreferences().edit {
            putLong("NO_OF_TABS", firebaseConfig.getLong("no_of_tabs"))
            putBoolean("MOVE_AVAILABLE", firebaseConfig.getBoolean("move_available"))
            putLong("ADD_BUTTON_PLACEMENT", firebaseConfig.getLong("add_button_placement"))
            putLong("MIN_SCAN_HOURS", firebaseConfig.getLong("min_scan_hours"))
            putLong("MIN_NUKE_HOURS", firebaseConfig.getLong("min_nuke_hours"))
            putLong("SETTING_PLACEMENT", firebaseConfig.getLong("setting_placement"))
            putBoolean("NOTIFICATION_VIEW_AVAILABLE", firebaseConfig.getBoolean("notification_view_available"))
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        dataRepo.destroy() // Remember to close Realm when done.
    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (requestCode === REQUEST_INVITE) {
//            if (resultCode === AppCompatActivity.RESULT_OK) {
//                data?.let {
//                    val ids = AppInviteInvitation.getInvitationIds(resultCode, data)
//                    for (id in ids) {
//                        d("onActivityResult: sent invitation " + id)
//                        firebaseAnalytics.logEvent("InvitationSent", null)
//                        // Get the invitation IDs of all sent messages
//                    }
//
//                }
//
//            } else {
//                // Sending failed or it was canceled, show failure message to the user
//                // ...
//                firebaseAnalytics.logEvent("InvitationSendFailed", null)
//            }
//        } else {
//
//            val tabsList = dataRepo.getAllTabsSorted()
//            val tabsAvailableAlready = tabsList.isNotEmpty()
//
//            val isResultOk = resultCode !== Activity.RESULT_CANCELED
//
//            val bundle = Bundle()
//            bundle.putBoolean("WatchResult", isResultOk)
//            bundle.putBoolean("TabsAvailable", tabsAvailableAlready)
//            firebaseAnalytics.logEvent("Watch Result", bundle)
//
//            val areTabsAvailable = isResultOk || tabsAvailableAlready
////            fab.setVisible(areTabsAvailable)
////            tabs.setVisible(areTabsAvailable)
////            firstMessageParent.setVisible(!areTabsAvailable)
//
//            if (isResultOk) {
//                mainActivityViewModel.handleActivityResult(requestCode, resultCode, data ?: Intent())
//            }
//        }
//
//    }
}
