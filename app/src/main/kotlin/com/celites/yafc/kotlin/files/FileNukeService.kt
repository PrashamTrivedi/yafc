package com.celites.yafc.kotlin.files

import android.app.PendingIntent
import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.support.v4.app.TaskStackBuilder
import android.support.v4.content.ContextCompat
import com.celites.kutils.edit
import com.celites.kutils.getDefaultSharedPreferences
import com.celites.kutils.setNotification
import com.celites.kutils.w
import com.celites.yafc.BuildConfig
import com.celites.yafc.R
import com.celites.yafc.kotlin.Utils.getFilesFromPath
import com.celites.yafc.kotlin.data.DataRepository
import com.celites.yafc.kotlin.files.receivers.DeleteReceiver
import com.celites.yafc.kotlin.files.receivers.NukeReceiver
import com.celites.yafc.kotlin.home.MainActivity
import com.google.firebase.crash.FirebaseCrash
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by Prasham on 8/18/2016.
 */
class FileNukeService : JobService() {
    override fun onStopJob(params: JobParameters?): Boolean {
        return true
    }

    override fun onStartJob(params: JobParameters?): Boolean {

        val path = params?.extras?.getString(getString(R.string.bundle_file_path)) ?: "Service"
        val name = params?.extras?.getString(getString(R.string.bundle_dir_name)) ?: "Nemo"
        val dataRepo = DataRepository().initWith(this)
        val tabModel = dataRepo.getTabByPath(path)
        tabModel?.let {
            (it.path ?: path).getFilesFromPath(this@FileNukeService).count().compose { it.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()) }
                    .subscribe(object : SingleObserver<Long> {


                        var count = 0L
                        override fun onSuccess(t: Long) {
                            count = t
                            if (count > 0) {
                                getDefaultSharedPreferences().edit {
                                    putString("group_nuke_$path", "$name: $count files")
                                }
                                setNotification(it.id, {
                                    setContentTitle(it.name)
                                    setAutoCancel(true)
                                    color = ContextCompat.getColor(this@FileNukeService, R.color.colorPrimary)
                                    val other = if (BuildConfig.DEBUG) "\n${it.path?.substringAfterLast("\\")}" else ""
                                    val contentMessage = "Time to clean ${it.name}, Click here to see the files"
                                    setContentText(contentMessage)
                                    setGroup("Nuke")
                                    setSortKey(it.id.toString())
                                    setSmallIcon(R.drawable.ic_delete_black_24dp)
                                    if (getDefaultSharedPreferences().getBoolean("NOTIFICATION_VIEW_AVAILABLE", false)) {
                                        val bundle = Bundle()
                                        bundle.putString(getString(R.string.bundle_selected_path), it.path)
                                        bundle.putBoolean(getString(R.string.bundle_generate_notifications), false)
                                        bundle.putString(getString(R.string.bundle_selection_type), getString(R.string.bundle_selection_all_files))
                                        val pendingIntent = getIntent(this@FileNukeService, bundle, it.id)
                                        setContentIntent(pendingIntent)
                                    }

                                    val intent = Intent(this@FileNukeService, DeleteReceiver::class.java)
                                    val pendingIntent = PendingIntent.getBroadcast(this@FileNukeService, it.id, intent, PendingIntent.FLAG_UPDATE_CURRENT)

                                    setDeleteIntent(pendingIntent)


                                    val deleteBundle = Bundle()
                                    deleteBundle.putString(getString(R.string.bundle_selected_path), it.path)
                                    deleteBundle.putString(getString(R.string.bundle_selection_type), getString(R.string.bundle_selection_all_files))
                                    deleteBundle.putString(getString(R.string.bundle_action), getString(R.string.bundle_action_delete))

                                    val deleteActionIntent = Intent(this@FileNukeService, NukeReceiver::class.java)
                                    deleteActionIntent.putExtras(deleteBundle)
                                    deleteActionIntent.action = getString(R.string.bundle_action_delete)
                                    val deleteIntent = PendingIntent.getBroadcast(this@FileNukeService, it.id, deleteActionIntent, PendingIntent.FLAG_UPDATE_CURRENT)
                                    addAction(R.drawable.ic_delete_black_24dp, "Delete", deleteIntent)


                                    val bigTextStyle = NotificationCompat.BigTextStyle(this)
                                    bigTextStyle.setBigContentTitle(it.name)
                                    val noOfFiles = "$count files"
                                    bigTextStyle.bigText(contentMessage)
                                    val message = if (BuildConfig.DEBUG) "$noOfFiles\n$other" else noOfFiles
                                    bigTextStyle.setSummaryText(message)

                                    setStyle(bigTextStyle)

                                    val closeBundle = Bundle()

                                    closeBundle.putString(getString(R.string.bundle_selected_path), it.path)
                                    closeBundle.putString(getString(R.string.bundle_selection_type), getString(R.string.bundle_selection_all_files))
                                    closeBundle.putString(getString(R.string.bundle_action), getString(R.string.bundle_action_not_now))

                                    val closeActionIntent = Intent(this@FileNukeService, NukeReceiver::class.java)
                                    closeActionIntent.putExtras(closeBundle)
                                    closeActionIntent.action = getString(R.string.bundle_action_not_now)
                                    val closeIntent = PendingIntent.getBroadcast(this@FileNukeService, it.id, closeActionIntent, PendingIntent.FLAG_UPDATE_CURRENT)
                                    addAction(R.drawable.ic_close_black_24dp, getString(R.string.action_not_now), closeIntent)
                                })

                                updateGroupNotification(name)
                            }
                        }

                        override fun onSubscribe(d: Disposable) {
                        }


                        override fun onError(e: Throwable) {
                            w("Error", e.toString())
                            FirebaseCrash.report(e)

                        }
                    })

        }
        jobFinished(params, false)
        return true
    }

    private fun getIntent(fileNukeService: FileNukeService, bundle: Bundle? = null, id: Int): PendingIntent? {
        val intent = Intent(fileNukeService, MainActivity::class.java)
        bundle?.let {
            intent.putExtras(it)
        }
        val stackBuilder: TaskStackBuilder = TaskStackBuilder.create(fileNukeService)
        stackBuilder.addNextIntent(intent)
        val pendingIntent = stackBuilder.getPendingIntent(id, PendingIntent.FLAG_UPDATE_CURRENT)
        return pendingIntent
    }

    private fun updateGroupNotification(name: String) {
        val pathKeys = getDefaultSharedPreferences().all.filterKeys { it.startsWith("group_nuke_") }
        setNotification(91212, {
            setContentTitle("Files to Delete")
            setSmallIcon(R.drawable.ic_delete_black_24dp)
            color = ContextCompat.getColor(this@FileNukeService, R.color.colorPrimary)
            setGroup("Nuke")
            setGroupSummary(true)
            val bundle = Bundle()
            val pendingIntent = getIntent(this@FileNukeService, bundle, 91212)
            setContentIntent(pendingIntent)
            setContentText("You have files to delete")
            if (pathKeys.isEmpty()) {
                val bigTextStyle = NotificationCompat.BigTextStyle(this)
                bigTextStyle.bigText("You have files to delete")
                setStyle(bigTextStyle)
            } else {
                val inboxStyle = NotificationCompat.InboxStyle(this)

                for ((_, value) in pathKeys) {
                    inboxStyle.addLine(value.toString())
                }
                inboxStyle.setBigContentTitle(name)
                inboxStyle.setSummaryText("You have files to delete")
                setStyle(inboxStyle)
            }

        })
    }

}