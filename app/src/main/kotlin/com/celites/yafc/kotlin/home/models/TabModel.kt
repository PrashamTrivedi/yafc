package com.celites.yafc.kotlin.home.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Prasham on 2/22/2016.
 */
open class TabModel(
        @PrimaryKey open var id: Int = 0, open
var path: String? = "", open var name: String = "", open var place: Int = 0, open var scanDurationInHours: Long = 12, open var nukeDurationInHours: Long = 24) : RealmObject() {


}