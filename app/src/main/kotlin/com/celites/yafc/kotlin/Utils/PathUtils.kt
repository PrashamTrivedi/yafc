package com.celites.yafc.kotlin.Utils

import android.content.Context
import android.net.Uri
import android.support.v4.provider.DocumentFile
import com.celites.kutils.isEmptyString
import io.reactivex.Flowable


/**
 * Created by Prasham on 9/17/2016.
 */
fun String.getFilesFromPath(context: Context, allowDirectories: Boolean = false) = Flowable.fromIterable(getFiles(context, allowDirectories))

fun String.getFiles(context: Context, allowDirectories: Boolean = false): List<DocumentFile> {
	if (!this.isEmptyString()) {
		val documentPathTree = this.getDocumentTree(context)
		val listFiles = documentPathTree.listFiles()
		return listFiles.filter {
			(allowDirectories || !it.isDirectory) && it.name != null && !it.name.equals(".nomedia", true) && !it.name.equals("debug.txt", true)
		}
	} else {
		return arrayListOf()
	}
}

fun String.getDocumentTree(context: Context): DocumentFile {
	val uri = Uri.parse(this)
	//    val documentPathTree = DocumentFile.fromTreeUri(context, uri)
	val c = Class.forName("android.support.v4.provider.TreeDocumentFile")
	val constructor = c.getDeclaredConstructor(DocumentFile::class.java, Context::class.java, Uri::class.java)
	constructor.isAccessible = true

	val dir = constructor.newInstance(null, context, uri) as DocumentFile
	return dir
}