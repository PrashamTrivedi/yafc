package com.celites.yafc.kotlin.Utils

import android.content.Context
import com.ceelites.sharedpreferenceinspector.Utils.SharedPreferenceUtils

/**
 * Created by Prash on 13-07-2017.
 */
fun Context.startDebugActivity() = SharedPreferenceUtils.startActivity(this)