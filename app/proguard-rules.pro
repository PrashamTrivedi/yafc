# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\android-sdks/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-dontwarn
-ignorewarnings
-dontwarn kotlin.jvm.internal.Intrinsics

# Subdirectory Solution
# From http://stackoverflow.com/questions/27759915/bug-when-listing-files-with-android-storage-access-framework-on-lollipop


##TESTING REMOVING
#-dontwarn kotlin.**
#
#-keepclassmembers class **$WhenMappings {
#    <fields>;
#}
#
## From http://stackoverflow.com/a/37113076
### Kotlin non null check improvement
#-assumenosideeffects class kotlin.jvm.internal.Intrinsics {
#    static void checkParameterIsNotNull(java.lang.Object, java.lang.String);
#}
#-keepclassmembers class **$WhenMappings {
#    <fields>;
#}

# Firebase Auth
-keepattributes Signature
-keepattributes *Annotation*

# IAP
-keep class com.android.vending.billing.**

-assumenosideeffects class org.solovyev.android.checkout.Billing {
    public static void debug(...);
    public static void warning(...);
    public static void error(...);
}

-assumenosideeffects class org.solovyev.android.checkout.Check {
    static *;
}